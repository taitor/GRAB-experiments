#!/bin/bash

METHOD=${1}
DATA_NAME=${2}

if [[ $METHOD =~ ^grab ]]; then
  DATA_TYPE="binarize"
else
if [[ $METHOD = l1lr ]]; then
  DATA_TYPE="fim"
else
  DATA_TYPE="binscale"
fi
fi

DIR=./${DATA_NAME}/${DATA_TYPE}/

function showScores() {
  for name in `ls ${DIR}${METHOD}*.txt`; do
    score=`cat ${name} | awk '{ sum += $1 } END { if (NR > 0) print sum / NR }'`
    echo ${name} ${score}
  done
}

showScores | awk '{if (m < $2) { n = $1; m = $2 } } END { print n " " m }'
