#!/bin/bash

METHOD=${1}

for data in `echo $(ls -d */) | sed 's/\///g'`; do
  ./pick_best.sh ${METHOD} ${data}
done
