#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Create Makefile downloading datasets
"""
import argparse
import os.path
import sys
import json

def main():
    """main function"""
    args = get_args()
    source = args['source']

    defs = read_json_or_die(source)

    lines = [
        'all: {}'.format(' '.join(defs.keys())),
        'clean:',
        '\trm -rf {}'.format(' '.join(defs.keys())),
        'PREPROCESS:=../src/preprocess.py',
        'EXPAND:=../src/expand',
        'SHUFFLE:=../src/shuffle.py',
        'MASTER_NAME=master.slf',
        'RAW_NAME=$@/raw/$(MASTER_NAME)',
        'RAW_DIR=$@/raw/',
        'BINARIZE_NAME=$@/binarize/$(MASTER_NAME)',
        'BINARIZATION_NAME=$@/binarize/binarization.txt',
        'BINARIZE_DIR=$@/binarize/',
        'BINSCALE_NAME=$@/binscale/$(MASTER_NAME)',
        'BINSCALE_DIR=$@/binscale/',
        'FIM_NAME=$@/fim/$(MASTER_NAME)',
        'FIM_DIR=$@/fim/'
    ]


    for key in defs:
        lines += get_lines(key, defs)

    with open('Makefile', 'w') as f:
        f.writelines([l + '\n' for l in lines])


def get_args():
    """Get argument variables"""
    parser = argparse.ArgumentParser()
    parser.add_argument('source')
    return vars(parser.parse_args())


def read_json_or_die(file_path):
    """Read json from file or die"""
    try:
        with open(file_path) as file:
            json_data = json.load(file)
    except json.JSONDecodeError as err:
        sys.exit(err)

    return json_data


def get_raw_name(key):
    """get raw name"""
    return '{}/raw/master.slf'.format(key)


def get_download_line(url, idx):
    """get download"""
    root, ext = os.path.splitext(url)
    line = 'curl {} | bunzip2'.format(url) if ext == '.bz2' else 'curl {}'.format(url)
    return '{} {} $@'.format(
        line, '>' if idx == 0 else '>>'
    )


def get_download_lines(urls, preprocess):
    """get download script"""
    lines = [
        get_download_line(u, i)
        for i, u in enumerate(urls)
    ]
    if preprocess is not None:
        lines.append('{} $@'.format(preprocess))
    return lines


def get_binarize_line(n_bins):
    """get binarize line"""
    if n_bins < 0:
        return 'cp $(RAW_NAME) $(BINARIZE_NAME)'
    return 'python3 $(PREPROCESS) -m binarize -b {} -d $(BINARIZATION_NAME) $(RAW_NAME) $(BINARIZE_NAME)'.format(n_bins)


def get_shuffle_lines(seed, trains, test):
    """get shuffle lines"""
    TEMPLATE = 'python3 $(SHUFFLE) -s {} --train {} --test {} {} {}'
    return [
        TEMPLATE.format(seed, ' '.join(map(lambda v: str(v), trains)), test, '$({}_NAME)'.format(m), '$({}_DIR)'.format(m))
        for m in ['BINARIZE', 'BINSCALE', 'FIM']
    ]


def get_generate_lines(d):
    """get generate lines"""
    n_trains = max(d['trains'])
    return [
        'mkdir -p $(BINARIZE_DIR) $(BINSCALE_DIR) $(FIM_DIR)',
        get_binarize_line(d['n_bins']),
        'python3 $(PREPROCESS) -m scale $(BINARIZE_NAME) $(BINSCALE_NAME)',
        f'$(EXPAND) -l 5 -f {n_trains//100} $(BINARIZE_NAME) $(FIM_NAME)'
    ] + get_shuffle_lines(d['seed'], d['trains'], d['test'])


def execute_lines(lines):
    """insert tabs"""
    return ['\t{}'.format(l) for l in lines]


def get_lines(key, defs):
    """get lines for key"""
    d = defs[key]   
    raw_name = get_raw_name(key)
    return [
        '{}:'.format(raw_name),
    ] + execute_lines(
        ['mkdir -p `dirname $@`'] +
        get_download_lines(d['urls'], d.get('preprocess'))
    ) + ['{}: {}'.format(key, raw_name)] + execute_lines(get_generate_lines(d))


if __name__ == '__main__':
   main()
