//
//  combination_counter.cpp
//
//  Created by otita on 2017/01/28.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <utility>

#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cerrno>

#include <unistd.h>

#include "slf_util.hpp"
#include "lcm_ext_counter.h"
#include "struct.h"

using namespace std;
using namespace otita;

uint64_t count_combination(const char ifname[], uint32_t l);

int main(int argc, char *argv[]) {
  int opt;

  bool lflag = false;
  uint32_t l = 0;

  const char *data_file_name, *output_file_name;

  while ((opt = getopt(argc, argv, "l:")) != -1) {
    switch (opt) {
      case 'l':
        lflag = true;
        int ll;
        ll = atoi(optarg);
        if (ll < 0) {
          cerr << "condition l >= 0 is not satisfied" << endl;
          goto USAGE;
        }
        l = ll;
        break;
      default: /* '?' */
        goto USAGE;
    }
  }

  if (!(lflag && optind + 1 < argc)) goto USAGE;

if (0) {
USAGE:
  cerr << "Usage: " << argv[0] << " -l l data_file_name result_file_name" << endl;
  exit(EXIT_FAILURE);
}

  data_file_name = argv[optind];
  output_file_name = argv[optind + 1];

  ofstream ofs(output_file_name, ios::out);
  if (!ofs.is_open()) {
    cerr << "cannot open output_file_name: " << output_file_name << endl;
    exit(EXIT_FAILURE);
  }

  uint64_t n_combinations = count_combination(data_file_name, l);

  ofs << "data=" << data_file_name << endl;
  ofs << "l=" << l << endl;
  ofs << "n_combinations=" << n_combinations << endl;

  return 0;
}

uint64_t count_combination(const char ifname[], uint32_t l) {
  vector<vector<pair<uint32_t, bool> > > x_storage;
  vector<double> y_storage;
  uint32_t n_row, n_col;
  read_slf(ifname, &x_storage, &y_storage, &n_row, &n_col);
  if (!(n_row && n_col)) {
    cerr << "cannot read data_file_name: " << ifname << endl;
    exit(EXIT_FAILURE);
  }

  weighted_itemsets wis;
  weighted_itemsets_init(&wis, 0, 0);
  for (auto pairs : x_storage) {
    weighted_itemsets_add_itemset(&wis, 1);
    for (auto pair : pairs) {
      if (pair.second) {
        uint32_t item = pair.first;
        weighted_itemsets_add_item(&wis, item);
      }
    }
  }

  return LCM_weighted_itemsets_count(&wis, 1, l);
}
