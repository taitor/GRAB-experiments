//
//  compare_model.cpp
//
//  Created by otita on 2017/01/31.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <unistd.h>
#include <iostream>
#include <string>
#include <set>
#include <utility>
#include <cstdint>
#include <cmath>

#include "grabc.hpp"
#include "slf_util.hpp"

using namespace std;
using namespace Eigen;
using namespace otita;
using namespace otita::grabc;

template <typename T>
void compare_models(
    const T &model1,
    const T &model2
);

set<set<uint32_t>> get_itemsets(const weighted_itemsets &wis);

double get_weight(const weighted_itemsets &wis);

double get_weight_of_difference(const weighted_itemsets &wis, const set<set<uint32_t>> &itemsets);

template <typename T>
set<T> difference(const set<T> &set1, const set<T> &set2);

int main(int argc, char *argv[]) {
  int opt;

  bool mflag = false;
  string method;

  while ((opt = getopt(argc, argv, "m:")) != -1) {
    switch (opt) {
      case 'm':
        mflag = true;
        method = optarg;
        if (!(method == "grab-lr" || method == "grab-svc")) {
          fprintf(stderr, "method must be in (grab-lr, grab-svc)");
          goto USAGE;
        }
        break;
      default: // ?
        goto USAGE;
    }
  }
  char *model_file1, *model_file2;

  if (!(mflag && optind + 1 < argc)) {
    goto USAGE;
  }

if (0) {
USAGE:
fprintf(stderr, "Usage: %s -m (grab-lr, grab-svc) model_file_name1 model_file_name2\n",
            argv[0]);
exit(EXIT_FAILURE);
}
  
  model_file1 = argv[optind];
  model_file2 = argv[optind + 1];

  double score = 0;
  if (method == "grab-lr") {
    LogisticRegression clf1(model_file1);
    LogisticRegression clf2(model_file2);
    compare_models(clf1, clf2);
  }
  else if (method == "grab-svc") {
    SVC clf1(model_file1);
    SVC clf2(model_file2);
    compare_models(clf1, clf2);
  }

  return 0;
}

template <typename T>
void compare_models(
  const T &model1,
  const T &model2
) {
  const weighted_itemsets &params1 = model1.get_params();
  const weighted_itemsets &params2 = model2.get_params();
  const set<set<uint32_t>> itemsets1 = get_itemsets(params1);
  const set<set<uint32_t>> itemsets2 = get_itemsets(params2);

  printf("|M1| = %lu\n", itemsets1.size());
  printf("|M2| = %lu\n", itemsets2.size());
  printf("|M1 - M2| = %lu\n", difference(itemsets1, itemsets2).size());
  printf("|M2 - M1| = %lu\n", difference(itemsets2, itemsets1).size());
  printf("||w1|M1| = %lf\n", get_weight(params1));
  printf("||w2|M2| = %lf\n", get_weight(params2));
  printf("||w1|(M1 - M2)| = %lf\n", get_weight_of_difference(params1, itemsets2));
  printf("||w2|(M2 - M1)| = %lf\n", get_weight_of_difference(params2, itemsets1));
}

set<set<uint32_t>> get_itemsets(const weighted_itemsets &wis) {
  const uint32_vector *items = &wis.items;
  const uint32_vector *indexes = &wis.indexes;
  const double_vector *weights = &wis.weights;
  set<set<uint32_t>> itemsets;
  for (int i = 0; i < weights->n; i++) {
    set<uint32_t> itemset;
    for (int j = i ? indexes->values[i - 1] : 0; j < indexes->values[i]; j++) {
      itemset.insert(items->values[j]);
    }
    itemsets.insert(itemset);
  }
  return itemsets;
}

double get_weight(const weighted_itemsets &wis) {
  const double_vector *weights = &wis.weights;
  double w = 0;
  for (int i = 0; i < weights->n; i++) {
    w += abs(weights->values[i]);
  }
  return w;
}

double get_weight_of_difference(const weighted_itemsets &wis, const set<set<uint32_t>> &itemsets) {
  const uint32_vector *items = &wis.items;
  const uint32_vector *indexes = &wis.indexes;
  const double_vector *weights = &wis.weights;
  double weight = 0;
  for (int i = 0; i < weights->n; i++) {
    double w = weights->values[i];
    set<uint32_t> itemset;
    for (int j = i ? indexes->values[i - 1] : 0; j < indexes->values[i]; j++) {
      itemset.insert(items->values[j]);
    }
    if (itemsets.find(itemset) == itemsets.end()) {
      weight += abs(w);
    }
  }
  return weight;
}

template <typename T>
set<T> difference(const set<T> &set1, const set<T> &set2) {
  set<T> result;
  for (const T &e: set1) {
    if (set2.find(e) == set2.end()) {
      result.insert(e);
    }
  }
  return result;
}
