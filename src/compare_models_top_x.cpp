//
//  compare_model2.cpp
//
//  Created by otita on 2017/01/31.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <unistd.h>
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <utility>
#include <algorithm>
#include <cstdint>
#include <cmath>

#include "grabc.hpp"
#include "slf_util.hpp"

using namespace std;
using namespace Eigen;
using namespace otita;
using namespace otita::grabc;

template <typename T>
void compare_models(
  const T &model1,
  const T &model2,
  const double x
);

template <typename T>
set<set<uint32_t>> get_top_x_percent_itemsets(
    const T &model,
    const double x
);

template <typename T>
vector<pair<double, set<uint32_t>>> get_weighted_itemsets(const T &model);

template <typename T>
set<T> difference(const set<T> &set1, const set<T> &set2);

int main(int argc, char *argv[]) {
  int opt;

  bool mflag = false;
  string method;
  bool xflag = false;
  double x;

  while ((opt = getopt(argc, argv, "m:x:")) != -1) {
    switch (opt) {
      case 'm':
        mflag = true;
        method = optarg;
        if (!(method == "grab-lr" || method == "grab-svc")) {
          fprintf(stderr, "method must be in (grab-lr, grab-svc)\n");
          goto USAGE;
        }
        break;
      case 'x':
        xflag = true;
        x = atof(optarg);
        if (x <= 0) {
          fprintf(stderr, "x > 0 is not satisfied\n");
        }
        break;
      default: // ?
        goto USAGE;
    }
  }
  char *model_file1, *model_file2;

  if (!(mflag && xflag && optind + 1 < argc)) {
    goto USAGE;
  }

if (0) {
USAGE:
fprintf(stderr, "Usage: %s -m (grab-lr, grab-svc) -x top_x model_file_name1 model_file_name2\n",
            argv[0]);
exit(EXIT_FAILURE);
}
  
  model_file1 = argv[optind];
  model_file2 = argv[optind + 1];

  double score = 0;
  if (method == "grab-lr") {
    LogisticRegression clf1(model_file1);
    LogisticRegression clf2(model_file2);
    compare_models(clf1, clf2, x);
  }
  else if (method == "grab-svc") {
    SVC clf1(model_file1);
    SVC clf2(model_file2);
    compare_models(clf1, clf2, x);
  }

  return 0;
}

template <typename T>
void compare_models(
  const T &model1,
  const T &model2,
  const double x
) {
  const auto &top_x1 = get_top_x_percent_itemsets(model1, x);
  const auto &top_x2 = get_top_x_percent_itemsets(model2, x);

  printf("x = %lf\n", x);
  printf("|top_x1| = %lu\n", top_x1.size());
  printf("|top_x2| = %lu\n", top_x2.size());
  printf("|top_x1 - top_x2| = %lu\n", difference(top_x1, top_x2).size());
  printf("|top_x2 - top_x1| = %lu\n", difference(top_x2, top_x1).size());
}

template <typename T>
set<set<uint32_t>> get_top_x_percent_itemsets(
    const T &model,
    const double x
) {
  auto wis = get_weighted_itemsets(model);
  sort(wis.begin(), wis.end(),
    [](const pair<double, set<uint32_t>> &a, const pair<double, set<uint32_t>> &b) {
      return abs(a.first) > abs(b.first);
    });
  double largest_weight = abs(wis[0].first);
  set<set<uint32_t>> res;
  for (const auto &e: wis) {
    if (abs(e.first) < largest_weight * x) {
      break;
    }
    res.insert(e.second);
  }
  return res;
}


template <typename T>
vector<pair<double, set<uint32_t>>> get_weighted_itemsets(const T &model) {
  const weighted_itemsets &wis = model.get_params();

  const uint32_vector *items = &wis.items;
  const uint32_vector *indexes = &wis.indexes;
  const double_vector *weights = &wis.weights;

  vector<pair<double, set<uint32_t>>> res;
  res.reserve(weights->n);
  for (int i = 0; i < weights->n; i++) {
    const double weight = weights->values[i];
    set<uint32_t> itemset;
    for (int j = i ? indexes->values[i - 1] : 0; j < indexes->values[i]; j++) {
      itemset.insert(items->values[j]);
    }
    res.push_back(make_pair(weight, itemset));
  }

  return res;
}

template <typename T>
set<T> difference(const set<T> &set1, const set<T> &set2) {
  set<T> result;
  for (const T &e: set1) {
    if (set2.find(e) == set2.end()) {
      result.insert(e);
    }
  }
  return result;
}
