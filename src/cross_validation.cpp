//
//  cross_validation.cpp
//
//  Created by otita on 2018/05/21.
//
/*
The MIT License (MIT)

Copyright (c) 2018 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <unistd.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include "random.hpp"
#include "grabc.hpp"
#include "slf_util.hpp"

using namespace std;
using namespace Eigen;
using namespace otita;
using namespace otita::grabc;

void load_data(
  const vector<vector<pair<uint32_t, bool> > > &x_storage,
  const vector<double> &y_storage,
  uint32_t n_rows,
  uint32_t n_cols,
  X &x_train,
  Y &y_train,
  X &x_test,
  Y &y_test,
  uint32_t cv, uint32_t k
);


int main(int argc, char *argv[]) {

  int opt;

  bool mflag = false;
  string method;
  bool Cflag = false;
  double C = 0;
  double Cp = 0;
  double Cn = 0;
  bool kflag = false;
  uint32_t k = 0;
  bool lflag = false;
  uint32_t l = 0;
  bool cvflag = false;
  uint32_t cv = 0;
  char *input_file, *output_file;

  while ((opt = getopt(argc, argv, "m:C:p:n:k:l:c:")) != -1) {
    switch (opt) {
      case 'm':
        mflag = true;
        method = optarg;
        if (!(method == "grab-lr" || method == "grab-svc")) {
          fprintf(stderr, "method must be in (grab-lr, grab-svc)");
          goto USAGE;
        }
        break;
      case 'C':
        Cflag = true;
        C = atof(optarg);
        if (C <= 0) {
          fprintf(stderr, "condition C > 0 is not satisfied\n");
          goto USAGE;
        }
        break;
      case 'p':
        Cp = atof(optarg);
        if (Cp <= 0) {
          fprintf(stderr, "condition Cp > 0 is not satisfied\n");
          goto USAGE;
        }
        break;
      case 'n':
        Cn = atof(optarg);
        if (Cn <= 0) {
          fprintf(stderr, "condition Cn > 0 is not satisfied\n");
          goto USAGE;
        }
        break;
      case 'k': {
        kflag = true;
        int kk = atoi(optarg);
        if (kk <= 0) {
          fprintf(stderr, "condition k > 0 is not satisfied\n");
          goto USAGE;
        }
        k = kk;
        break;
      }
      case 'l': {
        lflag = true;
        int ll = atoi(optarg);
        if (ll < 0) {
          fprintf(stderr, "condition l >= 0 is not satisfied\n");
          goto USAGE;
        }
        l = ll;
        break;
      }
      case 'c': {
        cvflag = true;
        int ccvv = atoi(optarg);
        if (ccvv < 2) {
          fprintf(stderr, "condition cv >= 2 is not satisfied\n");
          goto USAGE;
        }
        cv = ccvv;
        break;
      }
      default: /* '?' */
        goto USAGE;
    }
  }

  if (!(mflag && Cflag && kflag && lflag && lflag && cvflag && optind + 1 < argc)) {
    goto USAGE;
  }

if (0) {
USAGE:
fprintf(stderr, "usage: %s -m (grab-lr, grab-svc) -C C] [-p Cp] [-n Cn] -k #(new features at once) -l max_feature_length -c cross_validation data_file_name score_file_name\n",
            argv[0]);
exit(EXIT_FAILURE);
}
 
  input_file = argv[optind];
  output_file = argv[optind + 1];
  ofstream ofs(output_file);
  if (!ofs) {
    fprintf(stderr, "cannot open %s\n", output_file);
    exit(EXIT_FAILURE);
  }

  for (uint32_t c = 0; c < cv; c++) {
    X x_train, x_test; Y y_train, y_test;

    uint32_t n_rows, n_cols;
    vector<vector<pair<uint32_t, bool> > > x_storage;
    vector<double> y_storage;
    read_slf(input_file, &x_storage, &y_storage, &n_rows, &n_cols);

    load_data(x_storage, y_storage, n_rows, n_cols, x_train, y_train, x_test, y_test, cv, c);

    double score = 0;
    if (method == "grab-lr") {
      LogisticRegression clf(C, l, k, {Cp, Cn}, 0.01, 0);
      clf.fit(x_train, y_train);
      score = clf.score(x_test, y_test);
    }
    else if (method == "grab-svc") {
      SVC clf(C, l, k, {Cp, Cn}, 0.01, 0);
      clf.fit(x_train, y_train);
      score = clf.score(x_test, y_test);
    }
    ofs << score << endl;
  }

  return 0;
}


void load_data(
  const vector<vector<pair<uint32_t, bool> > > &x_storage,
  const vector<double> &y_storage,
  uint32_t n_rows,
  uint32_t n_cols,
  X &x_train,
  Y &y_train,
  X &x_test,
  Y &y_test,
  uint32_t cv,
  uint32_t k
) {
  // use rows in [test_from, test_to) as test data
  uint32_t n_batch = n_rows / cv;
  uint32_t test_from = k * n_batch;
  uint32_t test_to = (k == cv - 1) ? n_rows : (test_from + n_batch);
  uint32_t n_test = test_to - test_from;
  uint32_t n_train = n_rows - n_test;

  x_train = X(n_train, n_cols);
  y_train = Y(n_train);
  x_test = X(n_test, n_cols);
  y_test = Y(n_test);

  vector<Triplet<bool> > x_train_triplets, x_test_triplets;
  for (int i = 0; i < n_rows; i++) {
    auto &x_triplets = (test_from <= i && i < test_to) ? x_test_triplets : x_train_triplets;
    auto &y = (test_from <= i && i < test_to) ? y_test : y_train;
    uint32_t row = (i < test_from) ? i : (i < test_to) ? i - test_from : i - n_test;
    for (const auto &pair : x_storage[i]) {
      uint32_t col = pair.first;
      bool val = pair.second;

      x_triplets.push_back((Triplet<bool>(row, col, val)));
    }
    y(row) = y_storage[i] > 0 ? +1 : -1;
  }
  x_train.setFromTriplets(x_train_triplets.begin(), x_train_triplets.end());
  x_test.setFromTriplets(x_test_triplets.begin(), x_test_triplets.end());
}
