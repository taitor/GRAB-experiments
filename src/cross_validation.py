"""Cross-validate model"""

import time
import pickle
import argparse
import numpy as np
from scipy.sparse import csr_matrix, coo_matrix
from sklearn import svm, tree, ensemble
from sklearn.datasets import load_svmlight_file
from xgboost import XGBClassifier
from sklearn.linear_model import LogisticRegression


def load_data(
    x, y,
    cv, k
):
    # use rows in [test_from, test_to) as test data
    n_rows, n_cols = x.get_shape()
    n_batch = int(n_rows / cv)
    test_from = k * n_batch
    test_to = n_rows if k == cv - 1 else test_from + n_batch
    n_test = test_to - test_from
    n_train = n_rows - n_test

    x_coo = coo_matrix(x)

    train_rows, train_cols, train_vals = [], [], []
    test_rows, test_cols, test_vals = [], [], []

    for row, col, val in zip(x_coo.row, x_coo.col, x_coo.data):
        rows = test_rows if test_from <= row and row < test_to else train_rows
        cols = test_cols if test_from <= row and row < test_to else train_cols
        vals = test_vals if test_from <= row and row < test_to else train_vals

        i = (
            row if row < test_from
            else row - test_from if row < test_to
            else row - n_test
        )

        rows.append(i)
        cols.append(col)
        vals.append(val)

    def _build_matrix(vals, rows, cols, n_rows, n_cols):
        return coo_matrix((vals, (rows, cols)), shape=(n_rows, n_cols)).tocsr()

    train_x = _build_matrix(train_vals, train_rows, train_cols, n_train, n_cols)
    test_x = _build_matrix(test_vals, test_rows, test_cols, n_test, n_cols)

    train_labels = []
    test_labels = []
    for row, v in enumerate(y):
        labels = test_labels if test_from <= row and row < test_to else train_labels

        i = (
            row if row < test_from
            else row - test_from if row < test_to
            else row - n_test
        )

        labels.append(v)

    train_y = np.array(train_labels)
    test_y = np.array(test_labels)

    return train_x, train_y, test_x, test_y


def main():
    """Main function"""
    args = get_args()
    input_file_name = args['input_file_name']
    score_file_name = args['score_file_name']
    method = args['method']
    cv = int(args['cv'])

    with open(score_file_name, mode='w') as f:
        for c in range(cv):
            data = load_svmlight_file(input_file_name)
            x = data[0].astype(float)
            y = data[1].astype(int)

            train_x, train_y, test_x, test_y = load_data(x, y, cv, c)

            if method == 'svc':
                C = float(args['C'])
                kernel = args['kernel']

                if kernel == 'linear':
                    clf = svm.SVC(C=C, kernel=kernel)

                elif kernel == 'rbf':
                    gamma = args['gamma']
                    if gamma != 'auto':
                        gamma = float(gamma)

                    clf = svm.SVC(C=C, kernel=kernel, gamma=gamma)

                elif kernel == 'poly':
                    degree = int(args['degree'])
                    gamma = float(args['gamma'])
                    coef0 = 1 - gamma

                    clf = svm.SVC(C=C, kernel=kernel, degree=degree, gamma=gamma, coef0=coef0)

            elif method == 'tree':
                clf = tree.DecisionTreeClassifier(
                    random_state=0, max_depth=int(args['max_depth'])
                )

            elif method == 'forest':
                clf = ensemble.RandomForestClassifier(
                    random_state=0, n_estimators=int(args['n_estimators']),
                    max_depth=int(args['max_depth'])
                )

            elif method == 'xgboost':
                n = x.shape[0]
                reg_lambda = float(1) / (n * float(args['C']))
                clf = XGBClassifier(
                    max_depth=int(args['max_depth']),
                    reg_lambda=reg_lambda,
                    n_jobs=1,
                    random_state=0
                )
                
            elif method == 'l1lr':
                clf = LogisticRegression(
                    penalty='l1',
                    solver='liblinear',
                    random_state=0,
                    C=float(args['C']),
                    fit_intercept=False
                )

            clf.fit(train_x, train_y)

            score = clf.score(test_x, test_y)
            print(score, file=f)


def get_args():
    """Parse arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file_name')
    parser.add_argument('score_file_name')
    parser.add_argument('-C', '--C', required=False)
    parser.add_argument('-m', '--method', required=True)
    parser.add_argument('-k', '--kernel', required=False)
    parser.add_argument('-d', '--degree', required=False)
    parser.add_argument('-g', '--gamma', required=False)
    parser.add_argument('--n_estimators', required=False)
    parser.add_argument('--max_depth', required=False)
    parser.add_argument('-c', '--cv', required=True)
    return vars(parser.parse_args())


if __name__ == '__main__':
    main()
