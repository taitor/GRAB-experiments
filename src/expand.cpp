//
//  expand.cpp
//
//  Created by otita on 2017/01/28.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include <chrono>

#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cerrno>

#include <unistd.h>

#include "slf_util.hpp"
#include "lcm_ext.h"
#include "struct.h"

using namespace std;
using namespace otita;

chrono::milliseconds expand(const char ifname[], const char ofname[], uint32_t l, uint32_t f);

int main(int argc, char *argv[]) {
  int opt;

  bool lflag = false;
  uint32_t l = 0;

  uint32_t f = 1;

  bool oflag = false;
  const char *output_file_name;

  const char *data_file_name, *expanded_data_file_name;

  while ((opt = getopt(argc, argv, "l:f:o:")) != -1) {
    switch (opt) {
      case 'l':
        lflag = true;
        int ll;
        ll = atoi(optarg);
        if (ll < 0) {
          cerr << "condition l >= 0 is not satisfied" << endl;
          goto USAGE;
        }
        l = ll;
        break;
      case 'f':
        int ff;
        ff = atoi(optarg);
        if (ff < 1) {
          cerr << "condition f >= 1 is not satisfied" << endl;
          goto USAGE;
        }
        f = ff;
        break;
      case 'o':
        oflag = true;
        output_file_name = optarg;
        break;
      default: /* '?' */
        goto USAGE;
    }
  }

  if (!(lflag && optind + 1 < argc)) goto USAGE;

if (0) {
USAGE:
  cerr << "Usage: " << argv[0] << " -l l [-f minimum_frequency (default 1)] [-o result_file_name]  data_file_name expanded_data_file_name" << endl;
  exit(EXIT_FAILURE);
}

  data_file_name = argv[optind];
  expanded_data_file_name = argv[optind + 1];

  chrono::milliseconds ms = expand(data_file_name, expanded_data_file_name, l, f);

  if (oflag) {
    ofstream ofs(output_file_name, ios::out);
    if (!ofs.is_open()) {
      cerr << "cannot open output_file_name: " << output_file_name << endl;
      exit(EXIT_FAILURE);
    }

    ofs << "data=" << data_file_name << endl;
    ofs << "expanded data=" << expanded_data_file_name << endl;
    ofs << "l=" << l << endl;
    ofs << "milliseconds=" << ms.count() << endl;
  }

  return 0;
}

bool supset(uint32_t *x, uint32_t x_len, uint32_t *y, uint32_t y_len) {
  int x_pos = 0;
  for (int y_pos = 0; y_pos < y_len; y_pos++) {
    uint32_t e = y[y_pos];
    while (x[x_pos] != e) {
      x_pos++;
      if (x_pos == x_len) return false;
    }
  }
  return true;
}

chrono::milliseconds expand(const char ifname[], const char ofname[], uint32_t l, uint32_t f) {
  vector<vector<pair<uint32_t, bool> > > x_storage;
  vector<double> y_storage;
  uint32_t n_row, n_col;
  read_slf(ifname, &x_storage, &y_storage, &n_row, &n_col);
  if (!(n_row && n_col)) {
    cerr << "cannot read data_file_name: " << ifname << endl;
    exit(EXIT_FAILURE);
  }
  FILE *ofp = fopen(ofname, "wb");
  if (!ofp) {
    cerr << "cannot open expanded_data_file_name: " << ofname << endl;
    exit(EXIT_FAILURE);
  }

  weighted_itemsets wis;
  weighted_itemsets_init(&wis, 0, 0);
  for (auto pairs : x_storage) {
    weighted_itemsets_add_itemset(&wis, 1);
    for (auto pair : pairs) {
      if (pair.second) {
        uint32_t item = pair.first;
        weighted_itemsets_add_item(&wis, item);
      }
    }
  }
  weighted_itemsets basis;
  weighted_itemsets_init(&basis, 0, 0);

  auto start = chrono::system_clock::now();
  LCM_weighted_itemsets(&wis, &basis, f, l, 0);
  auto end = chrono::system_clock::now();
  uint32_t n_basis = basis.weights.n;
  for (uint32_t i = 0; i < n_row; i++) {
    fprintf(ofp, "%lf", y_storage[i]);
    uint32_t i_start = i ? wis.indexes.values[i - 1] : 0;
    uint32_t i_end = wis.indexes.values[i];
    uint32_t *x = wis.items.values + i_start;
    uint32_t x_len = i_end - i_start;
    for (uint32_t j = 0; j < n_basis; j++) {
      uint32_t j_start = j ? basis.indexes.values[j - 1] : 0;
      uint32_t j_end = basis.indexes.values[j];
      uint32_t *y = basis.items.values + j_start;
      uint32_t y_len = j_end - j_start;
      if (supset(x, x_len, y, y_len)) {
        uint32_t col = j + 1;
        fprintf(ofp, " %d:1", col);
      }
    }
    fprintf(ofp, "\n");
  }
  return chrono::duration_cast<std::chrono::milliseconds>(end - start);
}
