//
//  grab_miner.cpp
//
//  Created by otita on 2017/01/24.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace {

// if false, grab terminates.
bool grab_miner(
  weighted_itemsets *x_sets,
  weighted_itemsets *basis,
  unordered_map<vector<uint32_t>, bool> &basis_cache,
  int32_t *theta_power,
  uint32_t max_itemset_size,
  uint32_t min_solutions,
  double *Gnorm1_init,
  double eps
) {
  double_vector *weights = &x_sets->weights;

  uint32_t upper_limit = min_solutions * 100;
  bool reach_to_upper_limit = false;

  do {
    weighted_itemsets new_basis;
    weighted_itemsets_init(&new_basis, 0, 0);
    while (*theta_power >= 0) {
      LCM_weighted_itemsets(x_sets, &new_basis, 1 << *theta_power, max_itemset_size, upper_limit);
      uint32_t n_basis1 = new_basis.weights.n;
      
      for (int i = 0; i < weights->n; i++) {
        weights->values[i] *= -1;
      }
      
      LCM_weighted_itemsets(x_sets, &new_basis, 1 << *theta_power, max_itemset_size, upper_limit);
      uint32_t n_basis2 = new_basis.weights.n - n_basis1;
      reach_to_upper_limit = upper_limit && ((n_basis1 >= upper_limit) || (n_basis2 >= upper_limit));
      if (new_basis.weights.n) break;
      (*theta_power)--;
    }
    if ((*theta_power) < 0) {
      // no frequent basis
      return false;
    }

    // calculate Gnorm1_in_z
    double Gnorm1_in_z = 0;
    priority_queue<pair<double, vector<uint32_t> > > queue;
    for (int i = 0; i < new_basis.weights.n; i++) {
      double weight = new_basis.weights.values[i];
      int from = i ? new_basis.indexes.values[i - 1] : 0;
      int to = new_basis.indexes.values[i];
      vector<uint32_t> itemset; itemset.reserve(to - from);
      for (int idx = from; idx < to; idx++) {
        uint32_t item = new_basis.items.values[idx];
        itemset.push_back(item);
      }
      sort(itemset.begin(), itemset.end());
      if (basis_cache.find(itemset) == basis_cache.end()) {
        queue.push(make_pair(weight, itemset));
        Gnorm1_in_z += fmax(0, weight - 1);
      }
    }
    weighted_itemsets_free(&new_basis);

    // calculate Gnorm1_in_f
    double Gnorm1_in_f = 0;
    for (int i = 0; i < basis->weights.n; i++) {
      Gnorm1_in_f += basis->weights.values[i];
    }

    if (*Gnorm1_init < 0) {
      // calculate Gnorm1_init
      *Gnorm1_init = Gnorm1_in_z;
    }
    else if (Gnorm1_in_f + Gnorm1_in_z < eps * (*Gnorm1_init)) {
      // continue loop
      if (*theta_power) {
        (*theta_power)--;
      }
      else if (reach_to_upper_limit) {
        upper_limit *= 10;
        if (upper_limit > 10000000) {
          upper_limit = 0;
        }
      }
      else {
        return false;
      }
      continue;
    }

    // add basis
    double weights = 0;
    uint32_t count = 0;
    while (!queue.empty() && (count < min_solutions || weights + Gnorm1_in_f < eps * (*Gnorm1_init))) {
      weights += fmax(0, queue.top().first - 1);
      count++;
      weighted_itemsets_add_itemset(basis, 0);
      const auto &itemset = queue.top().second;
      for (const auto &item : itemset) {
        weighted_itemsets_add_item(basis, item);
      }
      basis_cache.insert(make_pair(move(itemset), true));
      queue.pop();
    }

    return true;

  } while (reach_to_upper_limit || (*theta_power) >= 0);
  return false;
}

} // anonymous
