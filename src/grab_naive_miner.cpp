//
//  grab_naive_miner.cpp
//
//  Created by otita on 2017/01/27.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace {

void naive_mining(
  weighted_itemsets *database,
  weighted_itemsets *new_basis,
  double theta,
  uint32_t max_itemset_size,
  uint32_t upper_limit
);

bool grab_miner(
  weighted_itemsets *x_sets,
  weighted_itemsets *basis,
  unordered_map<vector<uint32_t>, bool> &basis_cache,
  int32_t *theta_power,
  uint32_t max_itemset_size,
  uint32_t min_solutions,
  double *Gnorm1_init,
  double eps
) {
  uint32_t upper_limit = min_solutions * 100;
  bool reach_to_upper_limit = false;

  do {
    weighted_itemsets new_basis;
    weighted_itemsets_init(&new_basis, 0, 0);
    while (*theta_power >= 0) {
      naive_mining(x_sets, &new_basis, 1 << *theta_power, max_itemset_size, upper_limit);
      uint32_t n_basis = new_basis.weights.n;
      
      reach_to_upper_limit = upper_limit && n_basis >= upper_limit;

      if (new_basis.weights.n) break;
      (*theta_power)--;
    }
    if ((*theta_power) < 0) {
      // no frequent basis
      return false;
    }

    // calculate Gnorm1_in_z
    double Gnorm1_in_z = 0;
    priority_queue<pair<double, vector<uint32_t> > > queue;
    for (int i = 0; i < new_basis.weights.n; i++) {
      double weight = new_basis.weights.values[i];
      int from = i ? new_basis.indexes.values[i - 1] : 0;
      int to = new_basis.indexes.values[i];
      vector<uint32_t> itemset; itemset.reserve(to - from);
      for (int idx = from; idx < to; idx++) {
        uint32_t item = new_basis.items.values[idx];
        itemset.push_back(item);
      }
      sort(itemset.begin(), itemset.end());
      if (basis_cache.find(itemset) == basis_cache.end()) {
        queue.push(make_pair(weight, itemset));
        Gnorm1_in_z += fmax(0, weight - 1);
      }
    }
    weighted_itemsets_free(&new_basis);

    // calculate Gnorm1_in_f
    double Gnorm1_in_f = 0;
    for (int i = 0; i < basis->weights.n; i++) {
      Gnorm1_in_f += basis->weights.values[i];
    }

    if (*Gnorm1_init < 0) {
      // calculate Gnorm1_init
      *Gnorm1_init = Gnorm1_in_z;
    }
    else if (Gnorm1_in_f + Gnorm1_in_z < eps * (*Gnorm1_init)) {
      // continue loop
      if (*theta_power) {
        (*theta_power)--;
      }
      else if (reach_to_upper_limit) {
        upper_limit *= 10;
        if (upper_limit > 10000000) {
          upper_limit = 0;
        }
      }
      else {
        return false;
      }
      continue;
    }

    // add basis
    double weights = 0;
    uint32_t count = 0;
    while (!queue.empty() && (count < min_solutions || weights + Gnorm1_in_f < eps * (*Gnorm1_init))) {
      weights += fmax(0, queue.top().first - 1);
      count++;
      weighted_itemsets_add_itemset(basis, 0);
      const auto &itemset = queue.top().second;
      for (const auto &item : itemset) {
        weighted_itemsets_add_item(basis, item);
      }
      basis_cache.insert(make_pair(move(itemset), true));
      queue.pop();
    }

    return true;

  } while (reach_to_upper_limit || (*theta_power) >= 0);
  return false;
}

bool supset(
  uint32_t *x, uint32_t x_len,
  vector<uint32_t> y
) {
  if (x_len == 0) return y.size() == 0;
  int x_pos = 0;
  for (int y_pos = 0; y_pos < y.size(); y_pos++) {
    uint32_t e = y[y_pos];
    while (x[x_pos] != e) {
      x_pos++;
      if (x_pos == x_len) return false;
    }
  }
  return true;
}

void occur(
  weighted_itemsets *database,
  vector<uint32_t> &pattern,
  double *p_occur,
  double *n_occur
) {
  *p_occur = 0;
  *n_occur = 0;
  for (int i = 0; i < database->weights.n; i++) {
    int from = i ? database->indexes.values[i - 1] : 0;
    int to = database->indexes.values[i];
    uint32_t x_len = to - from;
    if (supset(database->items.values + from, x_len, pattern)) {
      double weight = database->weights.values[i];
      if (weight > 0) {
        *p_occur += weight;
      }
      else {
        *n_occur += weight;
      }
    }
  }
}

void backtrack(
  weighted_itemsets *database,
  weighted_itemsets *new_basis,
  double theta,
  uint32_t max_itemset_size,
  uint32_t upper_limit,
  vector<uint32_t> &current,
  double p_occur,
  double n_occur,
  uint32_t n_item
) {
  double weight = fabs(p_occur + n_occur);
  if (weight > theta) {
    weighted_itemsets_add_itemset(new_basis, weight);
    for (auto item : current) weighted_itemsets_add_item(new_basis, item);
  }

  for (int item = current.size() ? current.back() + 1 : 0; item < n_item; item++) {
    if ((max_itemset_size && current.size() >= max_itemset_size) ||
        (upper_limit && new_basis->weights.n >= upper_limit)) return;
    current.push_back(item);
    double p_occur, n_occur;
    occur(database, current, &p_occur, &n_occur);
    if (p_occur > 0 || n_occur < 0) {
      backtrack(database, new_basis, theta, max_itemset_size, upper_limit, current, p_occur, n_occur, n_item);
    }
    current.pop_back();
  }
}

void naive_mining(
  weighted_itemsets *database,
  weighted_itemsets *new_basis,
  double theta,
  uint32_t max_itemset_size,
  uint32_t upper_limit
) {
  uint32_t n_item = 0;
  for (int i = 0; i < database->items.n; i++) {
    uint32_t item = database->items.values[i];
    n_item = max(n_item, item + 1);
  }

  vector<uint32_t> current;
  double p_occur, n_occur;

  occur(database, current, &p_occur, &n_occur);
  if (p_occur > 0 || n_occur < 0) {
    backtrack(database, new_basis, theta, max_itemset_size, upper_limit, current, p_occur, n_occur, n_item);
  }
}

} // anonymous
