//
//  grab_optimizer.cpp
//
//  Created by otita on 2017/01/24.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <locale.h>

#include "struct.h"

#undef GETI
#define GETI(i) (y[i]+1)

namespace {

using schar = signed char;
template <class T> static inline void swap(T& x, T& y) { T t=x; x=y; y=t; }
#ifndef min
template <class T> static inline T min(T x,T y) { return (x<y)?x:y; }
#endif
#ifndef max
template <class T> static inline T max(T x,T y) { return (x>y)?x:y; }
#endif
template <class S, class T> static inline void clone(T*& dst, S* src, int n)
{
	dst = new T[n];
	memcpy((void *)dst,(void *)src,sizeof(T)*n);
}

class sparse_operator
{
public:
  static void axpy_ext(const double a, weighted_itemsets *x_sets_t, int j, double *y) {
    int from = j ? x_sets_t->indexes.values[j - 1] : 0;
    int to = x_sets_t->indexes.values[j];
    for (int i = from; i < to; i++) {
      int ind = x_sets_t->items.values[i];
      y[ind] += a;
    }
  }

  static void axpy_ext(const double a, weighted_itemsets *x_sets_t, double_vector *y_vec, int j, double *y) {
    int from = j ? x_sets_t->indexes.values[j - 1] : 0;
    int to = x_sets_t->indexes.values[j];
    for (int i = from; i < to; i++) {
      int ind = x_sets_t->items.values[i];
      y[ind] += a * y_vec->values[ind];
    }
  }
};

#undef GETI
#define GETI(i) (y[i]+1)
// To support weights for instances, use GETI(i) (i)

void solve_l1r_lr(
	weighted_itemsets *x_sets_t, weighted_itemsets *basis, double_vector *y_vec, double eps,
  double Gnorm1_init,
	double Cp, double Cn)
{
  int l = y_vec->n;
	int w_size = x_sets_t->weights.n;
	int j, s, newton_iter=0, iter=0;
	int max_newton_iter = 100;
	int max_iter = 1000;
	int max_num_linesearch = 20;
	int active_size;
	int QP_active_size;

	double nu = 1e-12;
	double inner_eps = 1;
	double sigma = 0.01;
	double w_norm, w_norm_new;
	double z, G, H;
//	double Gnorm1_init = -1.0; // Gnorm1_init is initialized at the first iteration
	double Gmax_old = INF;
	double Gmax_new, Gnorm1_new;
	double QP_Gmax_old = INF;
	double QP_Gmax_new, QP_Gnorm1_new;
	double delta, negsum_xTd, cond;

	int *index = new int[w_size];
	schar *y = new schar[l];
	double *Hdiag = new double[w_size];
	double *Grad = new double[w_size];
	double *wpd = new double[w_size];
	double *xjneg_sum = new double[w_size];
	double *xTd = new double[l];
	double *exp_wTx = new double[l];
	double *exp_wTx_new = new double[l];
	double *tau = new double[l];
	double *D = new double[l];

	double C[3] = {Cn,0,Cp};

	// Initial w can be set here.
//	for(j=0; j<w_size; j++)
//		w[j] = x_sets_t->weights.values[j];

	for(j=0; j<l; j++)
	{
		if(y_vec->values[j] > 0)
			y[j] = 1;
		else
			y[j] = -1;

		exp_wTx[j] = 0;
	}

	w_norm = 0;
	for(j=0; j<w_size; j++)
	{
    basis->weights.values[j] = 0;
		w_norm += fabs(x_sets_t->weights.values[j]);
		wpd[j] = x_sets_t->weights.values[j];
		index[j] = j;
		xjneg_sum[j] = 0;
    int from = j ? x_sets_t->indexes.values[j - 1] : 0;
    int to = x_sets_t->indexes.values[j];
    for (int i = from; i < to; i++) {
      int ind = x_sets_t->items.values[i];
      double val = 1;
      exp_wTx[ind] += x_sets_t->weights.values[j] * val;
      if (y[ind] == -1) {
        xjneg_sum[j] += C[GETI(ind)] * val;
      }
    }
	}
	for(j=0; j<l; j++)
	{
		exp_wTx[j] = exp(exp_wTx[j]);
		double tau_tmp = 1/(1+exp_wTx[j]);
		tau[j] = C[GETI(j)]*tau_tmp;
		D[j] = C[GETI(j)]*exp_wTx[j]*tau_tmp*tau_tmp;
	}

	while(newton_iter < max_newton_iter)
	{
		Gmax_new = 0;
		Gnorm1_new = 0;
		active_size = w_size;

		for(s=0; s<active_size; s++)
		{
			j = index[s];
			Hdiag[j] = nu;
			Grad[j] = 0;

			double tmp = 0;
      int from = j ? x_sets_t->indexes.values[j - 1] : 0;
      int to = x_sets_t->indexes.values[j];
      for (int i = from; i < to; i++) {
        int ind = x_sets_t->items.values[i];
        Hdiag[j] += D[ind];
        tmp += tau[ind];
      }
			Grad[j] = -tmp + xjneg_sum[j];

			double Gp = Grad[j]+1;
			double Gn = Grad[j]-1;
			double violation = 0;
			if(x_sets_t->weights.values[j] == 0)
			{
				if(Gp < 0)
					violation = -Gp;
				else if(Gn > 0)
					violation = Gn;
				//outer-level shrinking
				else if(Gp>Gmax_old/l && Gn<-Gmax_old/l)
				{
					active_size--;
					swap(index[s], index[active_size]);
					s--;
					continue;
				}
			}
			else if(x_sets_t->weights.values[j] > 0)
				violation = fabs(Gp);
			else
				violation = fabs(Gn);

			Gmax_new = max(Gmax_new, violation);
			Gnorm1_new += violation;
      basis->weights.values[j] = violation;
		}

//		if(newton_iter == 0)
//			Gnorm1_init = Gnorm1_new;

		if(Gnorm1_new <= eps*Gnorm1_init) break;

		iter = 0;
		QP_Gmax_old = INF;
		QP_active_size = active_size;

		for(int i=0; i<l; i++)
			xTd[i] = 0;

		// optimize QP over wpd
		while(iter < max_iter)
		{
			QP_Gmax_new = 0;
			QP_Gnorm1_new = 0;

			for(j=0; j<QP_active_size; j++)
			{
				int i = j+rand()%(QP_active_size-j);
				swap(index[i], index[j]);
			}

			for(s=0; s<QP_active_size; s++)
			{
				j = index[s];
				H = Hdiag[j];

				G = Grad[j] + (wpd[j]-x_sets_t->weights.values[j])*nu;
        int from = j ? x_sets_t->indexes.values[j - 1] : 0;
        int to = x_sets_t->indexes.values[j];
        for (int i = from; i < to; i++) {
          int ind = x_sets_t->items.values[i];
          G += D[ind] * xTd[ind];
        }

				double Gp = G+1;
				double Gn = G-1;
				double violation = 0;
				if(wpd[j] == 0)
				{
					if(Gp < 0)
						violation = -Gp;
					else if(Gn > 0)
						violation = Gn;
					//inner-level shrinking
					else if(Gp>QP_Gmax_old/l && Gn<-QP_Gmax_old/l)
					{
						QP_active_size--;
						swap(index[s], index[QP_active_size]);
						s--;
						continue;
					}
				}
				else if(wpd[j] > 0)
					violation = fabs(Gp);
				else
					violation = fabs(Gn);

				QP_Gmax_new = max(QP_Gmax_new, violation);
				QP_Gnorm1_new += violation;

				// obtain solution of one-variable problem
				if(Gp < H*wpd[j])
					z = -Gp/H;
				else if(Gn > H*wpd[j])
					z = -Gn/H;
				else
					z = -wpd[j];

				if(fabs(z) < 1.0e-12)
					continue;
				z = min(max(z,-10.0),10.0);

				wpd[j] += z;

				sparse_operator::axpy_ext(z, x_sets_t, j, xTd);
			}

			iter++;

			if(QP_Gnorm1_new <= inner_eps*Gnorm1_init)
			{
				//inner stopping
				if(QP_active_size == active_size)
					break;
				//active set reactivation
				else
				{
					QP_active_size = active_size;
					QP_Gmax_old = INF;
					continue;
				}
			}

			QP_Gmax_old = QP_Gmax_new;
		}

		if(iter >= max_iter)
			fprintf(stderr, "WARNING: reaching max number of inner iterations\n");

		delta = 0;
		w_norm_new = 0;
		for(j=0; j<w_size; j++)
		{
			delta += Grad[j]*(wpd[j]-x_sets_t->weights.values[j]);
			if(wpd[j] != 0)
				w_norm_new += fabs(wpd[j]);
		}
		delta += (w_norm_new-w_norm);

		negsum_xTd = 0;
		for(int i=0; i<l; i++)
			if(y[i] == -1)
				negsum_xTd += C[GETI(i)]*xTd[i];

		int num_linesearch;
		for(num_linesearch=0; num_linesearch < max_num_linesearch; num_linesearch++)
		{
			cond = w_norm_new - w_norm + negsum_xTd - sigma*delta;

			for(int i=0; i<l; i++)
			{
				double exp_xTd = exp(xTd[i]);
				exp_wTx_new[i] = exp_wTx[i]*exp_xTd;
				cond += C[GETI(i)]*log((1+exp_wTx_new[i])/(exp_xTd+exp_wTx_new[i]));
			}

			if(cond <= 0)
			{
				w_norm = w_norm_new;
				for(j=0; j<w_size; j++)
					x_sets_t->weights.values[j] = wpd[j];
				for(int i=0; i<l; i++)
				{
					exp_wTx[i] = exp_wTx_new[i];
					double tau_tmp = 1/(1+exp_wTx[i]);
					tau[i] = C[GETI(i)]*tau_tmp;
					D[i] = C[GETI(i)]*exp_wTx[i]*tau_tmp*tau_tmp;
				}
				break;
			}
			else
			{
				w_norm_new = 0;
				for(j=0; j<w_size; j++)
				{
					wpd[j] = (x_sets_t->weights.values[j]+wpd[j])*0.5;
					if(wpd[j] != 0)
						w_norm_new += fabs(wpd[j]);
				}
				delta *= 0.5;
				negsum_xTd *= 0.5;
				for(int i=0; i<l; i++)
					xTd[i] *= 0.5;
			}
		}

		// Recompute some info due to too many line search steps
		if(num_linesearch >= max_num_linesearch)
		{
			for(int i=0; i<l; i++)
				exp_wTx[i] = 0;

			for(int i=0; i<w_size; i++)
			{
				if(x_sets_t->weights.values[i]==0) continue;
				sparse_operator::axpy_ext(x_sets_t->weights.values[i], x_sets_t, i, exp_wTx);
			}

			for(int i=0; i<l; i++)
				exp_wTx[i] = exp(exp_wTx[i]);
		}

		if(iter == 1)
			inner_eps *= 0.25;

		newton_iter++;
		Gmax_old = Gmax_new;
	}

	if(newton_iter >= max_newton_iter)
		fprintf(stderr, "WARNING: reaching max number of iterations\n");

	// calculate objective value

	double v = 0;
	int nnz = 0;
	for(j=0; j<w_size; j++)
		if(x_sets_t->weights.values[j] != 0)
		{
			v += fabs(x_sets_t->weights.values[j]);
			nnz++;
		}
	for(j=0; j<l; j++)
		if(y[j] == 1)
			v += C[GETI(j)]*log(1+1/exp_wTx[j]);
		else
			v += C[GETI(j)]*log(1+exp_wTx[j]);

  printf("%lf\n", v);
	delete [] index;
	delete [] y;
	delete [] Hdiag;
	delete [] Grad;
	delete [] wpd;
	delete [] xjneg_sum;
	delete [] xTd;
	delete [] exp_wTx;
	delete [] exp_wTx_new;
	delete [] tau;
	delete [] D;
}

void solve_l1r_l2_svc(
	weighted_itemsets *x_sets_t, weighted_itemsets *basis, double_vector *y_vec, double eps,
  double Gnorm1_init,
	double Cp, double Cn)
{
	int l = y_vec->n;
	int w_size = x_sets_t->weights.n;
	int j, s, iter = 0;
	int max_iter = 1000;
	int active_size = w_size;
	int max_num_linesearch = 20;

	double sigma = 0.01;
	double d, G_loss, G, H;
	double Gmax_old = INF;
	double Gmax_new, Gnorm1_new;

	double d_old, d_diff;
	double loss_old, loss_new;
	double appxcond, cond;

	int *index = new int[w_size];
	schar *y = new schar[l];
	double *b = new double[l]; // b = 1-ywTx
	double *xj_sq = new double[w_size];

	double C[3] = {Cn,0,Cp};

	for(j=0; j<l; j++)
	{
		b[j] = 1;
		if(y_vec->values[j] > 0)
			y[j] = 1;
		else
			y[j] = -1;
	}
	for(j=0; j<w_size; j++)
	{
		index[j] = j;
		xj_sq[j] = 0;
    int from = j ? x_sets_t->indexes.values[j - 1] : 0;
    int to = x_sets_t->indexes.values[j];
    for (int i = from; i < to; i++) {
      int ind = x_sets_t->items.values[i];
      double val = 1 * y[ind];
      b[ind] -= x_sets_t->weights.values[j] * val;
      xj_sq[j] += C[GETI(ind)]*val*val;
    }
  }

	while(iter < max_iter)
	{
		Gmax_new = 0;
		Gnorm1_new = 0;

		for(j=0; j<active_size; j++)
		{
			int i = j+rand()%(active_size-j);
			swap(index[i], index[j]);
		}

		for(s=0; s<active_size; s++)
		{
			j = index[s];
			G_loss = 0;
			H = 0;

      int from = j ? x_sets_t->indexes.values[j - 1] : 0;
      int to = x_sets_t->indexes.values[j];
      for (int i = from; i < to; i++) {
        int ind = x_sets_t->items.values[i];
				if(b[ind] > 0)
				{
					double val = 1 * y[ind];
					double tmp = C[GETI(ind)]*val;
					G_loss -= tmp*b[ind];
					H += tmp*val;
				}
      }
			G_loss *= 2;

			G = G_loss;
			H *= 2;
			H = max(H, 1e-12);

			double Gp = G+1;
			double Gn = G-1;
			double violation = 0;
			if(x_sets_t->weights.values[j] == 0)
			{
				if(Gp < 0)
					violation = -Gp;
				else if(Gn > 0)
					violation = Gn;
				else if(Gp>Gmax_old/l && Gn<-Gmax_old/l)
				{
					active_size--;
					swap(index[s], index[active_size]);
					s--;
					continue;
				}
			}
			else if(x_sets_t->weights.values[j] > 0)
				violation = fabs(Gp);
			else
				violation = fabs(Gn);

			Gmax_new = max(Gmax_new, violation);
			Gnorm1_new += violation;
      basis->weights.values[j] = violation;

			// obtain Newton direction d
			if(Gp < H*x_sets_t->weights.values[j])
				d = -Gp/H;
			else if(Gn > H*x_sets_t->weights.values[j])
				d = -Gn/H;
			else
				d = -x_sets_t->weights.values[j];

			if(fabs(d) < 1.0e-12)
				continue;

			double delta = fabs(x_sets_t->weights.values[j]+d)-fabs(x_sets_t->weights.values[j]) + G*d;
			d_old = 0;
			int num_linesearch;
			for(num_linesearch=0; num_linesearch < max_num_linesearch; num_linesearch++)
			{
				d_diff = d_old - d;
				cond = fabs(x_sets_t->weights.values[j]+d)-fabs(x_sets_t->weights.values[j]) - sigma*delta;

				appxcond = xj_sq[j]*d*d + G_loss*d + cond;
				if(appxcond <= 0)
				{
          sparse_operator::axpy_ext(d_diff, x_sets_t, y_vec, j, b);
					break;
				}

				if(num_linesearch == 0)
				{
					loss_old = 0;
					loss_new = 0;
          int from = j ? x_sets_t->indexes.values[j - 1] : 0;
          int to = x_sets_t->indexes.values[j];
          for (int i = from; i < to; i++) {
            int ind = x_sets_t->items.values[i];
						if(b[ind] > 0)
							loss_old += C[GETI(ind)]*b[ind]*b[ind];
						double b_new = b[ind] + d_diff*y[ind];
						b[ind] = b_new;
						if(b_new > 0)
							loss_new += C[GETI(ind)]*b_new*b_new;
          }
				}
				else
				{
					loss_new = 0;
          int from = j ? x_sets_t->indexes.values[j - 1] : 0;
          int to = x_sets_t->indexes.values[j];
          for (int i = from; i < to; i++) {
            int ind = x_sets_t->items.values[i];
						double b_new = b[ind] + d_diff*y[ind];
						b[ind] = b_new;
						if(b_new > 0)
							loss_new += C[GETI(ind)]*b_new*b_new;

          }
				}

				cond = cond + loss_new - loss_old;
				if(cond <= 0)
					break;
				else
				{
					d_old = d;
					d *= 0.5;
					delta *= 0.5;
				}
			}

			x_sets_t->weights.values[j] += d;

			// recompute b[] if line search takes too many steps
			if(num_linesearch >= max_num_linesearch)
			{
//				info("#");
				for(int i=0; i<l; i++)
					b[i] = 1;

				for(int i=0; i<w_size; i++)
				{
					if(x_sets_t->weights.values[i]==0) continue;
          sparse_operator::axpy_ext(-x_sets_t->weights.values[i], x_sets_t, y_vec, i, b);
				}
			}
		}

//		if(iter == 0)
//			Gnorm1_init = Gnorm1_new;
		iter++;
//		if(iter % 10 == 0)
//			info(".");

		if(Gnorm1_new <= eps*Gnorm1_init)
		{
			if(active_size == w_size)
				break;
			else
			{
				active_size = w_size;
//				info("*");
				Gmax_old = INF;
				continue;
			}
		}

		Gmax_old = Gmax_new;
	}

//	fprintf(stderr, "\noptimization finished, #iter = %d\n", iter);
	if(iter >= max_iter)
		fprintf(stderr, "\nWARNING: reaching max number of iterations\n");

	// calculate objective value

	double v = 0;
	int nnz = 0;
	for(j=0; j<w_size; j++)
	{
		if(x_sets_t->weights.values[j] != 0)
		{
			v += fabs(x_sets_t->weights.values[j]);
			nnz++;
		}
	}
	for(j=0; j<l; j++)
		if(b[j] > 0)
			v += C[GETI(j)]*b[j]*b[j];

//	fprintf(stderr, "Objective value = %lf\n", v);
//	fprintf(stderr, "#nonzeros/#features = %d/%d\n", nnz, w_size);
  printf("%lf\n", v);

	delete [] index;
	delete [] y;
	delete [] b;
	delete [] xj_sq;
}

}
