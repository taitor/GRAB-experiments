//
//  grabc.cpp
//
//  Created by otita on 2016/11/29.
//
/*
The MIT License (MIT)

Copyright (c) 2016 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <stdexcept>
#include <iostream>
#include <fstream>
#include <sstream>
#include <queue>
#include <set>
#include <memory>
#include <unordered_map>
#include <algorithm>
#include <cmath>
#include <cstdlib>

#include "lcm_ext.h"

#include "grabc.hpp"

#define INF HUGE_VAL


#define FWRITE(type, val, fp) { \
  type tmp = type(val); \
  fwrite(&tmp, sizeof(type), 1, (fp)); \
}


#define FREAD(type, ptr, fp, exit) { \
  type buf; \
  if (fread(&buf, sizeof(type), 1, (fp)) != 1) { \
    goto exit; \
  } \
  *(ptr) = buf; \
}


using namespace std;
using namespace otita;
using namespace otita::grabc;

namespace {

constexpr array<uint32_t, 8> shifts = {0, 8, 16, 24, 32, 40, 48, 56, };

} // anonymous

namespace std {
template <>
struct hash<vector<uint32_t> > {
  size_t operator()(const vector<uint32_t> &vector) const {
    size_t hash = 0;
    size_t idx = 0;
    for (const auto &v : vector) {
      hash ^= v << shifts[idx++ & 7];
    }
    return hash;
  }
};

} // std

#include "grab_miner.cpp"
#include "grab_optimizer.cpp"

namespace {

void check(const X &x, const Y &y);
void update_phix_t(weighted_itemsets *x_sets, weighted_itemsets *basis, weighted_itemsets *phix_t);
void remove_unused_basis(weighted_itemsets *basis, unordered_map<vector<uint32_t>, bool> &basis_cache, weighted_itemsets *phix_t);
void remove_unused_basis(weighted_itemsets *basis, weighted_itemsets *phix_t);
bool supset(uint32_t *x, uint32_t x_len, uint32_t *y, uint32_t y_len);
void write_model(
  const char fname[],
  double C,
  uint32_t l,
  uint32_t k,
  const std::array<double, 2> &class_weights,
  double eps,
  uint32_t max_iteration,
  weighted_itemsets *params
);
void read_model(
  const char fname[],
  double *C,
  uint32_t *l,
  uint32_t *k,
  std::array<double, 2> *class_weights,
  double *eps,
  uint32_t *max_iteration,
  weighted_itemsets *params
);
void print_model(
  double C,
  uint32_t l,
  uint32_t k,
  const std::array<double, 2> &class_weights,
  double eps,
  uint32_t max_iteration,
  weighted_itemsets *params
);

void LogisticRegression_update_weights(weighted_itemsets *x_sets, double_vector *y_vec, weighted_itemsets *basis, weighted_itemsets *phix_t, double Cp, double Cn);
void LogisticRegression_init_x_sets_y_vec(const X &x, const Y &y, weighted_itemsets *x_sets, double_vector *y_vec, double Cp, double Cn);

void SVC_update_weights(weighted_itemsets *x_sets, double_vector *y_vec, weighted_itemsets *basis, weighted_itemsets *phix_t, double Cp, double Cn);
void SVC_init_x_sets_y_vec(const X &x, const Y &y, weighted_itemsets *x_sets, double_vector *y_vec, double Cp, double Cn);

} // anonymous

LogisticRegression::LogisticRegression(
  double C,
  uint32_t l,
  uint32_t k,
  const std::array<double, 2> &class_weights,
  double eps,
  uint32_t max_iteration
) : _C(C), _l(l), _k(k), _class_weights(class_weights), _eps(eps), _max_iteration(max_iteration) {
  if (_C <= 0) {
    throw invalid_argument("condition C>0 is not satisfied.");
  }
  if (_k <= 0) {
    throw invalid_argument("condition k>0 is not satisfied.");
  }
  if (!(_class_weights[0] == 0 && _class_weights[1] == 0)) {
    for (auto &w : _class_weights) {
      if (w <= 0) {
        throw invalid_argument("condition class_weight>0 is not satisfied.");
      }
    }
  }
  if (!(0 < _eps && _eps < 1)) {
    throw invalid_argument("condition 0 < eps < 1 is not satisfied.");
  }

  weighted_itemsets_init(&_params, 0, 0);
}

LogisticRegression::LogisticRegression(
  const char fname[]
) {
  read_model(fname, &_C, &_l, &_k, &_class_weights, &_eps, &_max_iteration, &_params);
  if (_C <= 0) {
    throw invalid_argument("condition C>0 is not satisfied.");
  }
  if (_k <= 0) {
    throw invalid_argument("condition k>0 is not satisfied.");
  }
  if (!(_class_weights[0] == 0 && _class_weights[1] == 0)) {
    for (auto &w : _class_weights) {
      if (w <= 0) {
        throw invalid_argument("condition class_weight>0 is not satisfied.");
      }
    }
  }
  if (!(0 < _eps && _eps < 1)) {
    throw invalid_argument("condition 0 < eps < 1 is not satisfied.");
  }
}

LogisticRegression::~LogisticRegression() {
  weighted_itemsets_free(&_params);
}

void LogisticRegression::fit(const X &x, const Y &y) {
  check(x, y);

  // initialize
  int32_t theta_power = 10;
  double Cp, Cn;
  if (_class_weights[0] == 0 && _class_weights[1] == 0) {
    uint32_t np = 0;
    uint32_t nn = 0;
    for (size_t i = 0; i < y.rows(); i++) {
      if (y(i) > 0) np++;
      else nn++;
    }
    uint32_t n = np + nn;
    double wp = double(n) / double(2 * np);
    double wn = double(n) / double(2 * nn);
    Cp = wp * _C;
    Cn = wn * _C;
  }
  else {
    Cp = _class_weights[0] * _C;
    Cn = _class_weights[1] * _C;
  }

  double  Gnorm1_init = -1.;

  double_vector y_vec;
  weighted_itemsets x_sets, basis, phix_t;
  unordered_map<vector<uint32_t>, bool> basis_cache;
  
  weighted_itemsets_init(&basis, 0, 0);
  weighted_itemsets_init(&phix_t, 0, 0);

  LogisticRegression_init_x_sets_y_vec(x, y, &x_sets, &y_vec, Cp, Cn);

  uint32_t n_iteration = 0;
  while (_max_iteration == 0 || n_iteration++ < _max_iteration) {
    if (!grab_miner(&x_sets, &basis, basis_cache, &theta_power, _l, _k, &Gnorm1_init, _eps)) break;

    update_phix_t(&x_sets, &basis, &phix_t);

    solve_l1r_lr(&phix_t, &basis, &y_vec, _eps, Gnorm1_init, Cp, Cn);

    remove_unused_basis(&basis, basis_cache, &phix_t);

    LogisticRegression_update_weights(&x_sets, &y_vec, &basis, &phix_t, Cp, Cn);
  }

  if (_max_iteration && n_iteration == _max_iteration)
    fprintf(stderr, "WARNING: reach to max iteration\n");

  // write basis to _params
  weighted_itemsets_free(&_params);
  weighted_itemsets_init(&_params, 0, 0);
  for (int i = 0; i < basis.weights.n; i ++) {
    double weight = phix_t.weights.values[i];
    if (weight == 0) {
      continue;
    }

    weighted_itemsets_add_itemset(&_params, weight);
    int from = i ? basis.indexes.values[i - 1] : 0;
    int to = basis.indexes.values[i];
    for (int j = from; j < to; j++) {
      uint32_t item = basis.items.values[j];
      weighted_itemsets_add_item(&_params, item);
    }
  }

  double_vector_free(&y_vec);
  weighted_itemsets_free(&basis);
  weighted_itemsets_free(&phix_t);
  weighted_itemsets_free(&x_sets);
}

Y LogisticRegression::predict(const X &x) {
  Y y(x.rows());
  Eigen::Matrix<double, -1, 2> log_proba = predict_log_proba(x);

  for (int i = 0; i < y.rows(); i++) {
    y(i) = log_proba(i, 0) > log(0.5) ? +1 : -1;
  }

  return y;
}

double LogisticRegression::score(const X &x, const Y &y) {
  check(x, y);
  Y pred_y = predict(x);
  int n = int(y.rows());
  int correct = 0;
  for (int i = 0; i < n; i++) {
    if (y(i) == pred_y(i)) {
      correct++;
    }
  }
  return double(correct) / double(n);
}

Eigen::Matrix<double, -1, 2> LogisticRegression::predict_log_proba(const X &x) {
  using namespace Eigen;

  Y y = Y::Zero(x.rows());
  double_vector y_vec;
  weighted_itemsets x_sets, phix_t;
  weighted_itemsets_init(&phix_t, 0, 0);
  LogisticRegression_init_x_sets_y_vec(x, y, &x_sets, &y_vec, 0, 0);

  update_phix_t(&x_sets, &_params, &phix_t);
  for (int i = 0; i < _params.weights.n; i++) phix_t.weights.values[i] = _params.weights.values[i];

  for (int d = 0; d < phix_t.weights.n; d++) {
    double weight = phix_t.weights.values[d];
    int from = d ? phix_t.indexes.values[d - 1] : 0;
    int to = phix_t.indexes.values[d];
    for (int idx = from; idx < to; idx++) {
      int i = phix_t.items.values[idx];
      y_vec.values[i] += weight;
    }
  }

  Matrix<double, -1, 2> log_proba(x.rows(), 2);
  for (int i = 0; i < y_vec.n; i++) {
    double f = y_vec.values[i];
    double pos_proba = 1. / (1. + exp(-f));
    log_proba(i, 0) = log(pos_proba);
    log_proba(i, 1) = log(1 - pos_proba);
  }

  double_vector_free(&y_vec);
  weighted_itemsets_free(&x_sets);
  weighted_itemsets_free(&phix_t);

  return log_proba;
}

void LogisticRegression::write_model(const char fname[]) {
  ::write_model(fname, _C, _l, _k, _class_weights, _eps, _max_iteration, &_params);
}

void LogisticRegression::print_model() {
  ::print_model(_C, _l, _k, _class_weights, _eps, _max_iteration, &_params);
}

SVC::SVC(
  double C,
  uint32_t l,
  uint32_t k,
  const std::array<double, 2> &class_weights,
  double eps,
  uint32_t max_iteration
) : _C(C), _l(l), _k(k), _class_weights(class_weights), _eps(eps), _max_iteration(max_iteration) {
  if (_C <= 0) {
    throw invalid_argument("condition C>0 is not satisfied.");
  }
  if (_k <= 0) {
    throw invalid_argument("condition k>0 is not satisfied.");
  }
  if (!(_class_weights[0] == 0 && _class_weights[1] == 0)) {
    for (auto &w : _class_weights) {
      if (w <= 0) {
        throw invalid_argument("condition class_weight>0 is not satisfied.");
      }
    }
  }
  if (!(0 < _eps && _eps < 1)) {
    throw invalid_argument("condition 0 < eps < 1 is not satisfied.");
  }

  weighted_itemsets_init(&_params, 0, 0);
}

SVC::SVC(
  const char fname[]
) {
  read_model(fname, &_C, &_l, &_k, &_class_weights, &_eps, &_max_iteration, &_params);
  if (_C <= 0) {
    throw invalid_argument("condition C>0 is not satisfied.");
  }
  if (_k <= 0) {
    throw invalid_argument("condition k>0 is not satisfied.");
  }
  if (!(_class_weights[0] == 0 && _class_weights[1] == 0)) {
    for (auto &w : _class_weights) {
      if (w <= 0) {
        throw invalid_argument("condition class_weight>0 is not satisfied.");
      }
    }
  }
  if (!(0 < _eps && _eps < 1)) {
    throw invalid_argument("condition 0 < eps < 1 is not satisfied.");
  }
}

SVC::~SVC() {
  weighted_itemsets_free(&_params);
}

void SVC::fit(const X &x, const Y &y) {
  check(x, y);

  // initialize
  int32_t theta_power = 10;
  double Cp, Cn;
  if (_class_weights[0] == 0 && _class_weights[1] == 0) {
    uint32_t np = 0;
    uint32_t nn = 0;
    for (size_t i = 0; i < y.rows(); i++) {
      if (y(i) > 0) np++;
      else nn++;
    }
    uint32_t n = np + nn;
    double wp = double(n) / double(2 * np);
    double wn = double(n) / double(2 * nn);
    Cp = wp * _C;
    Cn = wn * _C;
  }
  else {
    Cp = _class_weights[0] * _C;
    Cn = _class_weights[1] * _C;
  }

  double  Gnorm1_init = -1.;

  double_vector y_vec;
  weighted_itemsets x_sets, basis, phix_t;
  unordered_map<vector<uint32_t>, bool> basis_cache;

  weighted_itemsets_init(&basis, 0, 0);
  weighted_itemsets_init(&phix_t, 0, 0);

  SVC_init_x_sets_y_vec(x, y, &x_sets, &y_vec, Cp, Cn);

  uint32_t n_iteration = 0;
  while (_max_iteration == 0 || n_iteration++ < _max_iteration) {
    if (!grab_miner(&x_sets, &basis, basis_cache, &theta_power, _l, _k, &Gnorm1_init, _eps)) break;

    update_phix_t(&x_sets, &basis, &phix_t);

    solve_l1r_l2_svc(&phix_t, &basis, &y_vec, _eps, Gnorm1_init, Cp, Cn);

    remove_unused_basis(&basis, basis_cache, &phix_t);

    SVC_update_weights(&x_sets, &y_vec, &basis, &phix_t, Cp, Cn);
  }

  if (_max_iteration && n_iteration == _max_iteration)
    fprintf(stderr, "WARNING: reach to max iteration\n");

  // write basis to _params
  weighted_itemsets_free(&_params);
  weighted_itemsets_init(&_params, 0, 0);
  for (int i = 0; i < basis.weights.n; i ++) {
    double weight = phix_t.weights.values[i];
    if (weight == 0) {
      continue;
    }

    weighted_itemsets_add_itemset(&_params, weight);
    int from = i ? basis.indexes.values[i - 1] : 0;
    int to = basis.indexes.values[i];
    for (int j = from; j < to; j++) {
      uint32_t item = basis.items.values[j];
      weighted_itemsets_add_item(&_params, item);
    }
  }

  double_vector_free(&y_vec);
  weighted_itemsets_free(&basis);
  weighted_itemsets_free(&phix_t);
  weighted_itemsets_free(&x_sets);
}

Y SVC::predict(const X &x) {
  using namespace Eigen;

  Y y = Y::Zero(x.rows());
  double_vector y_vec;
  weighted_itemsets x_sets, phix_t;
  weighted_itemsets_init(&phix_t, 0, 0);
  SVC_init_x_sets_y_vec(x, y, &x_sets, &y_vec, 0, 0);

  update_phix_t(&x_sets, &_params, &phix_t);
  for (int i = 0; i < _params.weights.n; i++) phix_t.weights.values[i] = _params.weights.values[i];

  for (int d = 0; d < phix_t.weights.n; d++) {
    double weight = phix_t.weights.values[d];
    int from = d ? phix_t.indexes.values[d - 1] : 0;
    int to = phix_t.indexes.values[d];
    for (int idx = from; idx < to; idx++) {
      int i = phix_t.items.values[idx];
      y_vec.values[i] += weight;
    }
  }

  for (int i = 0; i < y.rows(); i++) y(i) = y_vec.values[i] > 0 ? +1 : -1;

  double_vector_free(&y_vec);
  weighted_itemsets_free(&x_sets);
  weighted_itemsets_free(&phix_t);

  return y;
}

double SVC::score(const X &x, const Y &y) {
  check(x, y);
  Y pred_y = predict(x);
  int n = int(y.rows());
  int correct = 0;
  for (int i = 0; i < n; i++) {
    if (y(i) == pred_y(i)) {
      correct++;
    }
  }
  return double(correct) / double(n);
}

void SVC::write_model(const char fname[]) {
  ::write_model(fname, _C, _l, _k, _class_weights, _eps, _max_iteration, &_params);
}

void SVC::print_model() {
  ::print_model(_C, _l, _k, _class_weights, _eps, _max_iteration, &_params);
}

namespace {

void check(const X &x, const Y &y) {
  // check arguments
  if (x.rows() != y.rows()) {
    throw invalid_argument("dimentions of x and y do not match.");
  }
  // check y
  for (auto i = 0; i < y.rows(); i++) {
    if (!(y(i) == 1 || y(i) == -1)) {
      throw invalid_argument("grab only accepts 1 or -1 as labels.");
    }
  }
}

void update_phix_t(weighted_itemsets *x_sets, weighted_itemsets *basis, weighted_itemsets *phix_t) {
  int j_end = basis->weights.n;
  int j_start = phix_t->weights.n;

  for (int j = j_start; j < j_end; j++) {
    int y_shift = j ? basis->indexes.values[j - 1] : 0;
    int y_len = basis->indexes.values[j] - y_shift;
    uint32_t *y = basis->items.values + y_shift;

    weighted_itemsets_add_itemset(phix_t, 0);

    for (int i = 0; i < x_sets->weights.n; i++) {
      int x_shift = i ? x_sets->indexes.values[i - 1] : 0;
      int x_len = x_sets->indexes.values[i] - x_shift;
      uint32_t *x = x_sets->items.values + x_shift;
      if (supset(x, x_len, y, y_len)) {
        weighted_itemsets_add_item(phix_t, i);
      }
    }
  }
}

#define COPY_WEIGHTED_ITEMSETS(copy, origin) \
  copy.items.values = origin.items.values; \
  copy.items.n = origin.items.n; \
  copy.items.n_buf = origin.items.n_buf; \
  copy.indexes.values = origin.indexes.values; \
  copy.indexes.n = origin.indexes.n; \
  copy.indexes.n_buf = origin.indexes.n_buf; \
  copy.weights.values = origin.weights.values; \
  copy.weights.n = origin.weights.n; \
  copy.weights.n_buf = origin.weights.n_buf; \

void remove_unused_basis(weighted_itemsets *basis, unordered_map<vector<uint32_t>, bool> &basis_cache, weighted_itemsets *phix_t) {
  weighted_itemsets old_basis, old_phix_t;
   COPY_WEIGHTED_ITEMSETS(old_basis, (*basis));
   COPY_WEIGHTED_ITEMSETS(old_phix_t, (*phix_t));
   weighted_itemsets_init(basis, old_basis.weights.n / 2, old_basis.items.n / 2);
   weighted_itemsets_init(phix_t, old_phix_t.weights.n / 2, old_phix_t.items.n / 2);
  for (int i = 0; i < old_phix_t.weights.n; i++) {
    double weight = old_phix_t.weights.values[i];
    int from, to;

    if (weight == 0) {
      from = i ? old_basis.indexes.values[i - 1] : 0;
      to = old_basis.indexes.values[i];
      vector<uint32_t> itemset; itemset.reserve(to - from);
      for (int j = from; j < to; j++) {
        uint32_t item = old_basis.items.values[j];
        itemset.push_back(item);
      }
      basis_cache.erase(itemset);
      continue;
    }

    from = i ? old_phix_t.indexes.values[i - 1] : 0;
    to = old_phix_t.indexes.values[i];
    weighted_itemsets_add_itemset(phix_t, weight);
    for (int j = from; j < to; j++) {
      uint32_t row = old_phix_t.items.values[j];
      weighted_itemsets_add_item(phix_t, row);
    }

    double weight_basis = old_basis.weights.values[i];
    from = i ? old_basis.indexes.values[i - 1] : 0;
    to = old_basis.indexes.values[i];
    weighted_itemsets_add_itemset(basis, weight_basis);
    for (int j = from; j < to; j++) {
      uint32_t item = old_basis.items.values[j];
      weighted_itemsets_add_item(basis, item);
    }
  }
  weighted_itemsets_free(&old_basis);
  weighted_itemsets_free(&old_phix_t);
}

void remove_unused_basis(weighted_itemsets *basis, weighted_itemsets *phix_t) {
  weighted_itemsets old_basis, old_phix_t;
   COPY_WEIGHTED_ITEMSETS(old_basis, (*basis));
   COPY_WEIGHTED_ITEMSETS(old_phix_t, (*phix_t));
   weighted_itemsets_init(basis, old_basis.weights.n / 2, old_basis.items.n / 2);
   weighted_itemsets_init(phix_t, old_phix_t.weights.n / 2, old_phix_t.items.n / 2);
  for (int i = 0; i < old_phix_t.weights.n; i++) {
    double weight = old_phix_t.weights.values[i];
    if (weight == 0) continue;
    int from, to;

    from = i ? old_phix_t.indexes.values[i - 1] : 0;
    to = old_phix_t.indexes.values[i];
    weighted_itemsets_add_itemset(phix_t, weight);
    for (int j = from; j < to; j++) {
      uint32_t row = old_phix_t.items.values[j];
      weighted_itemsets_add_item(phix_t, row);
    }

    from = i ? old_basis.indexes.values[i - 1] : 0;
    to = old_basis.indexes.values[i];
    weighted_itemsets_add_itemset(basis, weight);
    for (int j = from; j < to; j++) {
      uint32_t item = old_basis.items.values[j];
      weighted_itemsets_add_item(basis, item);
    }
  }
  weighted_itemsets_free(&old_basis);
  weighted_itemsets_free(&old_phix_t);
}

// return if x > y (x, y : ordered sets)
bool supset(uint32_t *x, uint32_t x_len, uint32_t *y, uint32_t y_len) {
  if (x_len == 0) return y_len == 0;
  int x_pos = 0;
  for (int y_pos = 0; y_pos < y_len; y_pos++) {
    uint32_t e = y[y_pos];
    while (x[x_pos] != e) {
      x_pos++;
      if (x_pos == x_len) return false;
    }
  }
  return true;
}

void write_model(
  const char fname[],
  double C,
  uint32_t l,
  uint32_t k,
  const std::array<double, 2> &class_weights,
  double eps,
  uint32_t max_iteration,
  weighted_itemsets *params
) {
  FILE *fp = fopen(fname, "wb");
  if (!fp) {
    fprintf(stderr, "cannot open %s.\n", fname);
    return;
  }

  FWRITE(double, C, fp);
  FWRITE(uint32_t, l, fp);
  FWRITE(uint32_t, k, fp);
  FWRITE(double, class_weights[0], fp);
  FWRITE(double, class_weights[1], fp);
  FWRITE(double, eps, fp);
  FWRITE(uint32_t, max_iteration, fp);

  priority_queue<pair<double, pair<bool, set<uint32_t> > > > queue;
  for (int j = 0; j < params->weights.n; j++) {
    double weight = params->weights.values[j];
    bool is_positive = (weight > 0);
    weight = fabs(weight);
    set<uint32_t> itemset;
    int from = j ? params->indexes.values[j - 1] : 0;
    int to = params->indexes.values[j];
    for (int d = from; d < to; d++) {
      uint32_t item = params->items.values[d];
      itemset.insert(item);
    }
    queue.push(make_pair(weight, make_pair(is_positive, itemset)));
  }

  while (!queue.empty()) {
    auto top = queue.top();
    double weight = top.second.first ? top.first : -top.first;
    auto itemset = top.second.second;

    FWRITE(double, weight, fp);
    FWRITE(size_t, itemset.size(), fp);
    for (uint32_t item : itemset) {
      FWRITE(uint32_t, item, fp);
    }

    queue.pop();
  }
  fclose(fp);
}

void read_model(
  const char fname[],
  double *C,
  uint32_t *l,
  uint32_t *k,
  std::array<double, 2> *class_weights,
  double *eps,
  uint32_t *max_iteration,
  weighted_itemsets *params
) {
  FILE *fp = fopen(fname, "rb");
  if (!fp) {
    throw invalid_argument("cannot open model file: " + string(fname) + ".");
  }

  FREAD(double, C, fp, ERROR);
  FREAD(uint32_t, l, fp, ERROR);
  FREAD(uint32_t, k, fp, ERROR);
  FREAD(double, (*class_weights).data(), fp, ERROR);
  FREAD(double, (*class_weights).data() + 1, fp, ERROR);
  FREAD(double, eps, fp, ERROR);
  FREAD(uint32_t, max_iteration, fp, ERROR);

  weighted_itemsets_init(params, 0, 0);
  while (1) {
    double weight;
    FREAD(double, &weight, fp, SUCCESS);
    weighted_itemsets_add_itemset(params, weight);
    size_t size;
    FREAD(size_t, &size, fp, ERROR);
    for (auto i = 0; i < size; i++) {
      uint32_t item;
      FREAD(uint32_t, &item, fp, ERROR);
      weighted_itemsets_add_item(params, item);
    }
  }

SUCCESS:
  fclose(fp);
  return;

ERROR:
  fclose(fp);
  throw invalid_argument("bad model file: " + string(fname) + ".");
}

// 0-indexed
void print_model(
  double C,
  uint32_t l,
  uint32_t k,
  const std::array<double, 2> &class_weights,
  double eps,
  uint32_t max_iteration,
  weighted_itemsets *params
) {
  printf(
    "C=%lf\n"
    "l=%d\n"
    "k=%d\n"
    "CWp=%lf\n"
    "CWn=%lf\n"
    "eps=%lf\n"
    "max_iteration=%d",
    C, l, k, class_weights[0], class_weights[1], eps, max_iteration
  );

  priority_queue<pair<double, pair<bool, set<uint32_t> > > > queue;
  for (int j = 0; j < params->weights.n; j++) {
    double weight = params->weights.values[j];
    bool is_positive = (weight > 0);
    weight = fabs(weight);
    set<uint32_t> itemset;
    int from = j ? params->indexes.values[j - 1] : 0;
    int to = params->indexes.values[j];
    for (int d = from; d < to; d++) {
      uint32_t item = params->items.values[d];
      itemset.insert(item);
    }
    queue.push(make_pair(weight, make_pair(is_positive, itemset)));
  }

  while (!queue.empty()) {
    printf("\n");
    auto top = queue.top();
    double weight = top.second.first ? top.first : -top.first;
    auto itemset = top.second.second;
    printf("%lf", weight);
    for (uint32_t item : itemset) {
      printf(" %d", item);
    }
    queue.pop();
  }
  printf("\n");
}

void LogisticRegression_update_weights(
  weighted_itemsets *x_sets,
  double_vector *y_vec,
  weighted_itemsets *basis,
  weighted_itemsets *phix_t,
  double Cp, double Cn
) {
  // initialize weights
  for (int i = 0; i < x_sets->weights.n; i++) {
    x_sets->weights.values[i] = 0;
  }
  for (int d = 0; d < phix_t->weights.n; d++) {
    double weight = phix_t->weights.values[d];
    int from = d ? phix_t->indexes.values[d - 1] : 0;
    int to = phix_t->indexes.values[d];
    for (int idx = from; idx < to; idx++) {
      int i = phix_t->items.values[idx];
      x_sets->weights.values[i] += weight;
    }
  }

  for (int i = 0; i < x_sets->weights.n; i++) {
    double weight = x_sets->weights.values[i];
    double y = y_vec->values[i];
    x_sets->weights.values[i] = ((y > 0) ? Cp : Cn) * y / (1 + exp(y * weight));
  }
}

void LogisticRegression_init_x_sets_y_vec(
  const X &x,
  const Y &y,
  weighted_itemsets *x_sets,
  double_vector *y_vec,
  double Cp, double Cn
) {
  using namespace Eigen;
  SparseMatrix<bool, RowMajor> xx = x;
  weighted_itemsets_init(x_sets, uint32_t(xx.outerSize()), uint32_t(xx.nonZeros()));
  for (int i = 0; i < xx.outerSize(); i++) {
    weighted_itemsets_add_itemset(x_sets, ((y(i) > 0) ? Cp : Cn) * y(i) * 0.5);
    for (SparseMatrix<bool, RowMajor>::InnerIterator it(xx, i); it; ++it) {
      if (it.value()) {
        uint32_t col = uint32_t(it.col());
        weighted_itemsets_add_item(x_sets, col);
      }
    }
  }
  double_vector_init(y_vec, uint32_t(y.rows()));
  for (int i = 0; i < y.rows(); i++) {
    double_vector_add(y_vec, y(i));
  }
}

void SVC_update_weights(
  weighted_itemsets *x_sets,
  double_vector *y_vec,
  weighted_itemsets *basis,
  weighted_itemsets *phix_t,
  double Cp, double Cn
) {
  // initialize weights
  for (int i = 0; i < x_sets->weights.n; i++) {
    x_sets->weights.values[i] = 0;
  }
  for (int d = 0; d < phix_t->weights.n; d++) {
    double weight = phix_t->weights.values[d];
    int from = d ? phix_t->indexes.values[d - 1] : 0;
    int to = phix_t->indexes.values[d];
    for (int idx = from; idx < to; idx++) {
      int i = phix_t->items.values[idx];
      x_sets->weights.values[i] += weight;
    }
  }

  for (int i = 0; i < x_sets->weights.n; i++) {
    double weight = x_sets->weights.values[i];
    double y = y_vec->values[i];
    weight = (1 - weight * y > 0) ? -2 * y * (1 - weight * y) : 0;
    x_sets->weights.values[i] = ((y > 0) ? Cp : Cn) * weight;
  }
}

void SVC_init_x_sets_y_vec(
  const X &x,
  const Y &y,
  weighted_itemsets *x_sets,
  double_vector *y_vec,
  double Cp, double Cn) {
  using namespace Eigen;
  SparseMatrix<bool, RowMajor> xx = x;
  weighted_itemsets_init(x_sets, uint32_t(xx.outerSize()), uint32_t(xx.nonZeros()));
  for (int i = 0; i < xx.outerSize(); i++) {
    weighted_itemsets_add_itemset(x_sets, ((y(i) > 0) ? Cp : Cn) * y(i) * (-2));
    for (SparseMatrix<bool, RowMajor>::InnerIterator it(xx, i); it; ++it) {
      if (it.value()) {
        uint32_t col = uint32_t(it.col());
        weighted_itemsets_add_item(x_sets, col);
      }
    }
  }
  double_vector_init(y_vec, uint32_t(y.rows()));
  for (int i = 0; i < y.rows(); i++) {
    double_vector_add(y_vec, y(i));
  }
}

} // anonymous
