//
//  grabc.hpp
//
//  Created by otita on 2016/11/29.
//
/*
The MIT License (MIT)

Copyright (c) 2016 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef _GRABC_HPP_
#define _GRABC_HPP_

#include <array>
#include <cstdint>
#include <Eigen/Dense>
#include <Eigen/SparseCore>

#include "struct.h"

namespace otita {

namespace grabc {

using X = Eigen::SparseMatrix<bool>;
using Y = Eigen::VectorXi;

class LogisticRegression final {
public:
  LogisticRegression(double C = 1, uint32_t l = 0, uint32_t k = 1, const std::array<double, 2> &class_weights = {0, 0}, double eps = 0.01, uint32_t max_iteration = 1000);
  LogisticRegression(const char fname[]);
  LogisticRegression(const LogisticRegression &other) = delete;
  LogisticRegression &operator=(const LogisticRegression &other) = delete;
  ~LogisticRegression();

  double get_C() const {
    return _C;
  }

  uint32_t get_l() const {
    return _l;
  }

  uint32_t get_k() const {
    return _k;
  }

  std::array<double, 2> get_class_weights() const {
    return _class_weights;
  }

  double get_eps() const {
    return _eps;
  }

  uint32_t get_max_iteration() const {
    return _max_iteration;
  }

  weighted_itemsets get_params() const {
    return _params;
  }

  void fit(const X &x, const Y &y);
  Y predict(const X &x);
  double score(const X &x, const Y &y);
  Eigen::Matrix<double, -1, 2> predict_log_proba(const X &x);

  void write_model(const char fname[]);
  void print_model();
private:
  double _C;
  uint32_t _l; // max length of basis functions
  uint32_t _k; // #(new features at once)
  std::array<double, 2> _class_weights; // { pos_weight, neg_weight };
  double _eps;
  uint32_t _max_iteration;

  weighted_itemsets _params;
};

class SVC final {
public:
  SVC(double C = 1, uint32_t l = 0, uint32_t k = 1, const std::array<double, 2> &class_weights = {0, 0}, double eps = 0.01, uint32_t max_iteration = 1000);
  SVC(const char fname[]);
  SVC(const SVC &other) = delete;
  SVC &operator=(const SVC &other) = delete;
  ~SVC();

  double get_C() const {
    return _C;
  }

  uint32_t get_l() const {
    return _l;
  }

  uint32_t get_k() const {
    return _k;
  }

  std::array<double, 2> get_class_weights() const {
    return _class_weights;
  }

  double get_eps() const {
    return _eps;
  }

  uint32_t get_max_iteration() const {
    return _max_iteration;
  }

  weighted_itemsets get_params() const {
    return _params;
  }

  void fit(const X &x, const Y &y);
  Y predict(const X &x);
  double score(const X &x, const Y &y);

  void write_model(const char fname[]);
  void print_model();
private:
  double _C;
  uint32_t _l; // max length of basis functions
  uint32_t _k; // #(new features at once)
  std::array<double, 2> _class_weights; // { pos_weight, neg_weight };
  double _eps;
  uint32_t _max_iteration;

  weighted_itemsets _params;
};

} // grabc

} // otita

#endif // _GRABC_HPP_
