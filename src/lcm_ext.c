//
//  lcm_ext.c
//
//  Created by otita on 2017/01/09.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#define WEIGHT_DOUBLE
#define TRSACT_DEFAULT_WEIGHT 1

#define LCM_UNCONST 16777216   // use the complement graph of the constraint graph
#define LCM_POSI_EQUISUPP 33554432   // an item will be dealt as "equisupp" when "positive"-frequency is equal to the positive-frequency of the current itemset 

#define ERROR_RET

#include <stdio.h>
#include <stdlib.h>

#include "itemset.c"
#include "trsact.c"
#include "sgraph.c"
#include "problem.c"

#include "struct.h"

#include "lcm_ext.h"

static void PROBLEM_load_ext(PROBLEM *P, weighted_itemsets *wis);
static void TRSACT_load_ext(TRSACT *T, weighted_itemsets *wis);
static void TRSACT_file_count_ext(TRSACT *T, FILE_COUNT *C, weighted_itemsets *wis);
static void TRSACT_file_read_ext(TRSACT *T, weighted_itemsets *wis, FILE_COUNT *C, VEC_ID *t, int flag);
static int FILE2_read_item_ext(LONG item, LONG *x, LONG *y, int fc, int flag);
static void ITEMSET_check_all_rule_ext(ITEMSET *I, WEIGHT *w, QUEUE *occ, QUEUE *jump, WEIGHT total, int core_id, weighted_itemsets *r_wis);
static void ITEMSET_solution_ext(ITEMSET *I, QUEUE *occ, int core_id, weighted_itemsets *r_wis);
static void ITEMSET_solution_iter_ext(ITEMSET *I, QUEUE *occ, int core_id, weighted_itemsets *r_wis);
static void ITEMSET_output_itemset_ext(ITEMSET *I, QUEUE *occ, int core_id, weighted_itemsets *r_wis);
static void ITEMSET_output_frequency_ext(ITEMSET *I, int core_id, weighted_itemsets *r_wis);

static void LCM_add_item(PROBLEM *PP, QUEUE *Q, QUEUE_INT item);
static void LCM_del_item(PROBLEM *PP, QUEUE *Q);
static void LCM_reduce_occ_by_posi_equisupp(PROBLEM *PP, QUEUE *occ, QUEUE_INT item, QUEUE_INT full);
static QUEUE_INT LCM_maximality_check(PROBLEM *PP, QUEUE *occ, QUEUE_INT item, QUEUE_INT *fmax, QUEUE_INT *cnt);
static void LCM(PROBLEM *PP, int item, QUEUE *occ, WEIGHT frq, WEIGHT pfrq, weighted_itemsets *r_wis);
static void LCM_init(PROBLEM *PP);

void PROBLEM_load_ext(PROBLEM *P, weighted_itemsets *wis) {
  TRSACT_load_ext(&P->TT, wis);
  if (ERROR_MES) {
    PROBLEM_end(P);
    EXIT;
  }
}

void TRSACT_load_ext(TRSACT *T, weighted_itemsets *wis) {
  FILE_COUNT C = INIT_FILE_COUNT;
  VEC_ID t = 0;
  int f;

  TRSACT_file_count_ext(T, &C, wis);
  if (ERROR_MES) goto END;
  T->end1 = T->rows_org;

  f = TRSACT_alloc(T, &C);
  if (ERROR_MES) {
    mfree (C.clmt, C.cperm, C.rowt, C.cw);
    goto END;
  }
  TRSACT_file_read_ext(T, wis, &C, &t, f);
  if (ERROR_MES) goto END;
  TRSACT_sort(T, &C, f);

END:
  if (ERROR_MES) TRSACT_end(T);
  else TRSACT_prop_print(T);
  return;
}

void TRSACT_file_count_ext(TRSACT *T, FILE_COUNT *C, weighted_itemsets *wis) {
  QUEUE_INT i, item, kk = 0, k, jump_end = 0;
  WEIGHT w, s;
  VEC_ID *jump = NULL;
  LONG jj;

  int n_itemset = wis->weights.n;
  kk = (QUEUE_INT)n_itemset;
  kk += T->rows_org;
  realloc2(C->rw, kk + 1, goto ERR);
  for (int i = 0; i < wis->weights.n; i++) C->rw[i] = wis->weights.values[i];
  ARY_MIN(w, i, C->rw, 0, kk);
  if (w < 0) T->flag2 |= TRSACT_NEGATIVE;
  for (int row = 0; row < n_itemset; row++) {
    s = 0;
    k = 0;
    w = C->rw[row];
    for (int idx = row ? wis->indexes.values[row - 1] : 0; idx < wis->indexes.values[row]; idx++) {
      jj = wis->items.values[idx];
      item = (QUEUE_INT)jj;
      if (jj < TRSACT_MAXNUM && jj >= 0) {
        ENMAX(T->clms_org, item + 1);
        reallocx(jump, jump_end, k, 0, goto ERR);
        jump[k] = item;
        k++;
        s += item<kk? MAX(C->rw[item],0): TRSACT_DEFAULT_WEIGHT;

        reallocx(C->clmt, C->clm_end, item, 0, goto ERR);
        C->clmt[item]++;
        if ( !(T->flag&LOAD_TPOSE) ){
          reallocx (C->cw, C->cw_end, item, 0, goto ERR);
          C->cw[item] += MAX(w,0);    // sum up positive weights
        }
      }
    }

    reallocx(C->rowt, C->row_end, T->rows_org, 0, goto ERR);
    C->rowt[T->rows_org] = k;
    if ( T->flag&LOAD_TPOSE ){
      reallocx (C->cw, C->cw_end, T->rows_org, 0, goto ERR);
      C->cw[T->rows_org] = s;    // sum up positive weights
    }
    T->rows_org++;

    if (k == 0) {
      T->str_num++;
    }
    else {
      T->eles_org += k;
      if ( (!(T->flag&LOAD_TPOSE) && !RANGE (T->row_lb, k, T->row_ub))
          || ((T->flag&LOAD_TPOSE) && (!RANGE(T->w_lb, s, T->w_ub) || !RANGE (T->clm_lb, k, T->clm_ub)) ) ) FLOOP (i, 0, k) C->clmt[jump[i]]--;
    }
  }
  free2(jump);
  
  if ( C->rw == NULL ){ T->total_w_org = T->total_pw_org = T->rows_org; return; } 
  C->clm_btm = MIN(kk, T->rows_org);
  reallocx (C->rw, kk, T->rows_org, TRSACT_DEFAULT_WEIGHT, goto ERR);
  FLOOP (k, 0, T->rows_org){
    T->total_w_org += C->rw[k];
    T->total_pw_org += MAX(C->rw[k],0);
  }
  return;
  ERR:;
  mfree (C->rw, C->cw, C->clmt, C->rowt, jump);
  EXIT;
}

void TRSACT_file_read_ext(TRSACT *T, weighted_itemsets *wis, FILE_COUNT *C, VEC_ID *t, int flag) {
  QUEUE_ID tt;
  LONG x, y;
  int fc=0, FILE_err_=0;

  if (flag) T->T.v[0].v = T->T.buf;

  int idx = 0;
#if 0
  int n_items = wis->items.n;
  do {
    if (flag) {
      if ( C->rperm[*t] < T->rows_org ){
        if ( C->rperm[*t] > 0 ) {
          T->T.v[C->rperm[*t]].v = T->T.v[C-> rperm[*t]-1].v + T->T.v[C->rperm[*t]-1].t +1;
        }
      }
    }

      x = *t;
      if (idx >= wis->indexes.values[*t]) {
        // read no number
        goto LOOP_END;
      }
      FILE_err_ = FILE2_read_item_ext(wis->items.values[idx], &x, &y, fc, T->flag);

      if ( C->rperm[x]<=T->rows_org && C->cperm[y]<=T->clms_end ){
        QUE_INS (T->T.v[ C->rperm[x] ], C->cperm[y]);
      }
    idx++;
    if (idx >= wis->indexes.values[*t]){
      LOOP_END:;
      (*t)++;
      fc = FILE_err_? 0: 1; FILE_err_=0; // even if next weight is not written, it is the rest of the previous line
    }

  } while (idx < n_items);
#else
  do {
    if (flag) {
      if ( C->rperm[*t] < T->rows_org ){
        if ( C->rperm[*t] > 0 ) {
          T->T.v[C->rperm[*t]].v = T->T.v[C-> rperm[*t]-1].v + T->T.v[C->rperm[*t]-1].t +1;
        }
      }
    }

      x = *t;
      if (idx >= wis->indexes.values[*t]) {
        // read no number
        goto LOOP_END;
      }
      FILE_err_ = FILE2_read_item_ext(wis->items.values[idx], &x, &y, fc, T->flag);

      if ( C->rperm[x]<=T->rows_org && C->cperm[y]<=T->clms_end ){
        QUE_INS (T->T.v[ C->rperm[x] ], C->cperm[y]);
      }
    idx++;
    if (idx >= wis->indexes.values[*t]){
      LOOP_END:;
      (*t)++;
      fc = FILE_err_? 0: 1; FILE_err_=0; // even if next weight is not written, it is the rest of the previous line
    }

  } while (*t < T->rows_org);
#endif


  FLOOP (tt, 0, T->T.t) T->T.v[tt].v[T->T.v[tt].t] = T->T.clms;
  mfree (C->rowt, C->clmt);
}

int FILE2_read_item_ext(LONG item, LONG *x, LONG *y, int fc, int flag) {
  int ff = 0;
  *y = item;
  if (flag & LOAD_ID1){ (*y)--; (*x)--; }

  if ((flag & LOAD_TPOSE) || ((flag&LOAD_EDGE) && *x > *y)) SWAP_LONG (*x, *y);
  return (ff);
}

void ITEMSET_check_all_rule_ext(ITEMSET *I, WEIGHT *w, QUEUE *occ, QUEUE *jump, WEIGHT total, int core_id, weighted_itemsets *r_wis){
#if 1
  WEIGHT d = I->frq/total;
    // checking out of range for itemset size and (posi/nega) frequency
  if ( I->itemset.t+I->add.t < I->lb || I->itemset.t>I->ub || (!(I->flag&ITEMSET_ALL) && I->itemset.t+I->add.t>I->ub)) return;
  if ( !(I->flag&ITEMSET_IGNORE_BOUND) && (I->frq < I->frq_lb || I->frq > I->frq_ub) ) return;
  if ( !(I->flag&ITEMSET_IGNORE_BOUND) && (I->pfrq < I->posi_lb || I->pfrq > I->posi_ub || (I->frq - I->pfrq) > I->nega_ub || (I->frq - I->pfrq) < I->nega_lb) ) return;

    // constraint of relational frequency
  if ( ((I->flag&ITEMSET_RFRQ)==0 || d >= I->prob_lb * I->prob ) 
      && ((I->flag&ITEMSET_RINFRQ)==0 || d <= I->prob * I->prob_ub) ){
    ITEMSET_solution_ext(I, occ, core_id, r_wis);
  }
#else
  QUEUE_ID i, t;
  QUEUE_INT e, f=0, *x;
  WEIGHT d = I->frq/total;

    // checking out of range for itemset size and (posi/nega) frequency
  if ( I->itemset.t+I->add.t < I->lb || I->itemset.t>I->ub || (!(I->flag&ITEMSET_ALL) && I->itemset.t+I->add.t>I->ub)) return;
  if ( !(I->flag&ITEMSET_IGNORE_BOUND) && (I->frq < I->frq_lb || I->frq > I->frq_ub) ) return;
  if ( !(I->flag&ITEMSET_IGNORE_BOUND) && (I->pfrq < I->posi_lb || I->pfrq > I->posi_ub || (I->frq - I->pfrq) > I->nega_ub || (I->frq - I->pfrq) < I->nega_lb) ) return;

  if ( I->flag&ITEMSET_SET_RULE ){  // itemset->itemset rule for sequence mining
    FLOOP (i, 0, I->itemset.t-1){
      if ( I->frq/I->set_weight[i] >= I->setrule_lb && I->fp ){
        I->sc[i]++;
        if (I->flag & ITEMSET_SC2) I->sc2[(QUEUE_INT)I->frq]++;  // histogram for LAMP
        if ( I->flag2 & ITEMSET_LAMP ) ITEMSET_lamp (I, 1);  // LAMP mode
        if ( I->flag2 & ITEMSET_LAMP2 ) ITEMSET_lamp2 (I, 1);  // 2D LAMP mode
        if ( I->flag&ITEMSET_PRE_FREQ ) ITEMSET_output_frequency (I, core_id);
        FLOOP (t, 0, I->itemset.t){
          FILE2_print_int (&I->multi_fp[core_id], I->itemset.v[t], t?I->separator:0);
          if ( t == i ){
            FILE2_putc (&I->multi_fp[core_id], ' ');
            FILE2_putc (&I->multi_fp[core_id], '=');
            FILE2_putc (&I->multi_fp[core_id], '>');
          }
        }
        if ( !(I->flag&ITEMSET_PRE_FREQ) ) ITEMSET_output_frequency ( I, core_id);
        FILE2_putc (&I->multi_fp[core_id], ' ');
        FILE2_print_real (&I->multi_fp[core_id], I->frq/I->set_weight[i], I->digits, '(');
        FILE2_putc (&I->multi_fp[core_id], ')');
#ifdef _FILE2_LOAD_FROM_MEMORY_
  FILE2_WRITE_MEMORY (QUEUE_INT, FILE2_LOAD_FROM_MEMORY_END);
#else
        FILE2_putc (&I->multi_fp[core_id], '\n');
#endif
#ifdef _trsact_h_
        if ( I->flag&(ITEMSET_TRSACT_ID+ITEMSET_MULTI_OCC_PRINT) ){
//            ITEMSET_output_occ (I, I->set_occ[i], core_id);
        }
#endif
        ITEMSET_flush (I, &I->multi_fp[core_id]);
      }
    }
  }
    // constraint of relational frequency
  if ( ((I->flag&ITEMSET_RFRQ)==0 || d >= I->prob_lb * I->prob ) 
      && ((I->flag&ITEMSET_RINFRQ)==0 || d <= I->prob * I->prob_ub) ){
    if ( I->flag&ITEMSET_RULE ){  //  rule mining routines
      if ( I->itemset.t == 0 ) return;
      if ( I->target < I->item_max ){
        MQUE_FLOOP (*jump, x){
          if ( *x == I->target ){ 
              ITEMSET_check_rule (I, w, occ, *x, core_id);   if (ERROR_MES) return;
          }
        }
//        ITEMSET_check_rule (I, w, occ, I->target, core_id);    if (ERROR_MES) return;
      } else {
        if ( I->flag & (ITEMSET_RULE_FRQ + ITEMSET_RULE_RFRQ) ){
          if ( I->add.t>0 ){
//            if ( I->itemflag[I->add.v[0]] ) // for POSI_EQUISUPP (occ_w[e] may not be 100%, in the case)
            f = I->add.v[I->add.t-1]; t = I->add.t; I->add.t--;
            FLOOP (i, 0, t){
              e = I->add.v[i];
              I->add.v[i] = f;
              ITEMSET_check_rule (I, w, occ, e, core_id);    if (ERROR_MES) return;
              I->add.v[i] = e;
            }
            I->add.t++;
          }
          MQUE_FLOOP (*jump, x)
              ITEMSET_check_rule (I, w, occ, *x, core_id);   if (ERROR_MES) return;
        } else {
          if ( I->flag & (ITEMSET_RULE_INFRQ + ITEMSET_RULE_RINFRQ) ){
//          ARY_FLOOP ( *jump, i, e ) I->itemflag[e]--;
            FLOOP (i, 0, I->item_max){
              if ( I->itemflag[i] != 1 ){
                ITEMSET_check_rule (I, w, occ, i, core_id);     if (ERROR_MES) return;
              }
            }
//          ARY_FLOOP ( *jump, i, e ) I->itemflag[e]++;
//        } 
//        ARY_FLOOP ( *jump, i, e ) ITEMSET_check_rule (I, w, occ, e);
          }
        }
      }
    } else {  // usual mining (not rule mining)
      if ( I->fp && (I->flag&(ITEMSET_RFRQ+ITEMSET_RINFRQ))){
        FILE2_print_real (&I->multi_fp[core_id], d, I->digits, '[');
        FILE2_print_real (&I->multi_fp[core_id], I->prob, I->digits, ',');
        FILE2_putc (&I->multi_fp[core_id], ']');
      }
      ITEMSET_solution_ext(I, occ, core_id, r_wis);
    }
  }
#endif
}

void ITEMSET_solution_ext(ITEMSET *I, QUEUE *occ, int core_id, weighted_itemsets *r_wis){
  QUEUE_ID i;
  LONG s;
  if ( I->itemset.t > I->ub ) return;
  if ( I->flag & ITEMSET_ALL ){
    if ( I->fp || I->topk.v ) ITEMSET_solution_iter_ext(I, occ, core_id, r_wis);
    else {
      s=1; FLOOP (i, 0, I->add.t+1){
        I->sc[I->itemset.t+i] += s;
        s = s*(I->add.t-i)/(i+1);
      }
      if (I->flag & ITEMSET_SC2){
        s = 1<<I->add.t;
        I->sc2[(QUEUE_INT)I->frq] += s;  // histogram for LAMP
        if ( I->flag2 & ITEMSET_LAMP ) ITEMSET_lamp (I, s);  // LAMP mode
        if ( I->flag2 & ITEMSET_LAMP2 ) ITEMSET_lamp2 (I, s);  // 2D LAMP mode
        else if ( I->topk_k > 0 && I->frq > I->topk_frq ){ // top-k histogram version
          while (1){
            if ( I->sc2[I->topk_frq] > s ){ I->sc2[I->topk_frq] -= s; break; }
            s -= I->sc2[I->topk_frq];
            I->sc2[I->topk_frq++] = 0; 
          }
          I->frq_lb = I->topk_frq+1;
        }
      }
    }
  } else {
    FLOOP (i, 0, I->add.t) QUE_INS (I->itemset, I->add.v[i]);
    ITEMSET_output_itemset_ext(I, occ, core_id, r_wis);
    I->itemset.t -= I->add.t;
  }
}

void ITEMSET_solution_iter_ext(ITEMSET *I, QUEUE *occ, int core_id, weighted_itemsets *r_wis){
  QUEUE_ID t=I->add.t;
  if ( I->itemset.t > I->ub ) return;
  ITEMSET_output_itemset_ext(I, occ, core_id, r_wis);
if ( ERROR_MES ) return;
  BLOOP (I->add.t, I->add.t, 0){
    QUE_INS (I->itemset, I->add.v[I->add.t]);
    ITEMSET_solution_iter_ext(I, occ, core_id, r_wis);
if ( ERROR_MES ) return;
    I->itemset.t--;
  }
  I->add.t = t;
}

void ITEMSET_output_itemset_ext(ITEMSET *I, QUEUE *occ, int core_id, weighted_itemsets *r_wis) {
  QUEUE_ID i;
  QUEUE_INT e;
#ifdef _agraph_h_
  QUEUE_INT ee;
#endif

//  FILE2 *fp = &I->multi_fp[core_id];
  
  I->multi_outputs[core_id]++;
  if ( (I->flag&SHOW_PROGRESS ) && (I->multi_outputs[core_id]%(ITEMSET_INTERVAL) == 0) )
      print_err ("---- " LONGF " solutions in " LONGF " candidates\n",
                  I->multi_solutions[core_id], I->multi_outputs[core_id]);
  if ( I->itemset.t < I->lb || I->itemset.t > I->ub ) return;
  if ( (I->flag&ITEMSET_IGNORE_BOUND)==0 && (I->frq < I->frq_lb || I->frq > I->frq_ub) ) return;
  if ( (I->flag&ITEMSET_IGNORE_BOUND)==0 && (I->pfrq < I->posi_lb || I->pfrq > I->posi_ub || (I->frq - I->pfrq) > I->nega_ub || (I->frq - I->pfrq) < I->nega_lb) ) return;

  I->multi_solutions[core_id]++;
  if ( I->max_solutions>0 && I->multi_solutions[core_id] > I->max_solutions ){
    ITEMSET_last_output (I);
    ERROR_MES = "reached to maximum number of solutions";
    EXIT;
  }

  I->sc[I->itemset.t]++;
  if (I->flag & ITEMSET_SC2) I->sc2[(QUEUE_INT)I->frq]++;  // histogram for LAMP

  if ( I->flag2 & ITEMSET_LAMP ){ ITEMSET_lamp (I, 1); return; }  // LAMP mode
  if ( I->flag2 & ITEMSET_LAMP2 ){ ITEMSET_lamp2 (I, 1); return; }  // 2D LAMP mode
  if ( I->itemtopk_end > 0 ){
    e = AHEAP_findmin_head (&(I->itemtopk[I->itemtopk_item]));
    if ( I->frq > AHEAP_H (I->itemtopk[I->itemtopk_item], e) ){
      AHEAP_chg (&(I->itemtopk[I->itemtopk_item]), e, I->frq * I->itemtopk_sign);
      if ( I->itemtopk_ary ) I->itemtopk_ary[I->itemtopk_item][e] = I->itemtopk_item2;
    }
    return;
  }

  if ( I->topk_k > 0 ){
    if ( I->topk.v ){
      e = AHEAP_findmin_head (&(I->topk));
      if ( I->frq * I->topk_sign > AHEAP_H (I->topk, e) ){
        AHEAP_chg (&(I->topk), e, I->frq * I->topk_sign);
        e = AHEAP_findmin_head (&(I->topk));
        I->frq_lb = AHEAP_H (I->topk, e) * I->topk_sign;
      }
    } else {  // histogram version
      if ( I->frq > I->topk_frq ){
        I->sc2[I->topk_frq]--;
        while (I->sc2[I->topk_frq]==0) I->topk_frq++;
        I->frq_lb = I->topk_frq+1;
      }
    }
    return;
  }
  
  if ( I->fp ){
    if ( I->flag&ITEMSET_PRE_FREQ ) ITEMSET_output_frequency_ext(I, core_id, r_wis);
    if ( (I->flag & ITEMSET_NOT_ITEMSET) == 0 ){
#ifdef _agraph_h_
      if ( I->flag&ITEMSET_OUTPUT_EDGE ){
        FLOOP (i, 0, I->itemset.t){
          e = I->itemset.v[i];
          ee = AGRAPH_INC_FROM(*((AGRAPH *)(I->X)), e, I->dir);
          FILE2_print_int (fp, I->perm? I->perm[ee]: ee, '(' );
          ee = AGRAPH_INC_TO(*((AGRAPH *)(I->X)), e, I->dir);
          FILE2_print_int (fp, I->perm? I->perm[ee]: ee, I->separator);
#ifdef _FILE2_LOAD_FROM_MEMORY_
          FILE2_putc (fp, ')');
#endif
          if ( i<I->itemset.t-1 ) FILE2_putc (fp, I->separator);
          if ( (i+1)%256==0 ) ITEMSET_flush (I, fp);
        }
        goto NEXT;
      }
#endif
      FLOOP (i, 0, I->itemset.t){
        e = I->itemset.v[i];
        weighted_itemsets_add_item(r_wis, I->perm? I->perm[e]: e);
//        FILE2_print_int (fp,  I->perm? I->perm[e]: e, i==0? 0: I->separator);
//        if ( (i+1)%256==0 ) ITEMSET_flush (I, fp);
      }
#ifdef _agraph_h_
      NEXT:;
#endif
    }
    if ( !(I->flag&ITEMSET_PRE_FREQ) ) ITEMSET_output_frequency_ext(I, core_id, r_wis);
    if ( ((I->flag & ITEMSET_NOT_ITEMSET) == 0) || (I->flag&ITEMSET_FREQ) || (I->flag&ITEMSET_PRE_FREQ) ){
#ifdef _FILE2_LOAD_FROM_MEMORY_
  FILE2_WRITE_MEMORY (QUEUE_INT, FILE2_LOAD_FROM_MEMORY_END);
#else
//      FILE2_putc (fp, '\n');
#endif
    }
//#ifdef _trsact_h_
//    if (I->flag&(ITEMSET_TRSACT_ID+ITEMSET_MULTI_OCC_PRINT)) ITEMSET_output_occ (I, occ, core_id);
//#endif
//    ITEMSET_flush (I, fp);
  }
}

void ITEMSET_output_frequency_ext(ITEMSET *I, int core_id, weighted_itemsets *r_wis){
//  FILE2 *fp = &I->multi_fp[core_id];
  if ( I->flag&(ITEMSET_FREQ+ITEMSET_PRE_FREQ) ){
    weighted_itemsets_add_itemset(r_wis, I->frq);
//    if ( I->flag&ITEMSET_FREQ ) FILE2_putc (fp, ' ');
//    FILE2_print_WEIGHT (fp, I->frq, I->digits, '(');
//    FILE2_putc (fp, ')');
//    if ( I->flag&ITEMSET_PRE_FREQ ) FILE2_putc (fp, ' ');
  }
  if ( I->flag&ITEMSET_OUTPUT_POSINEGA ){ // output positive sum, negative sum in the occurrence
//    FILE2_putc (fp, ' ');
//    FILE2_print_WEIGHT (fp, I->pfrq, I->digits, '(');
//    FILE2_print_WEIGHT (fp, I->pfrq-I->frq, I->digits, ',');
//    FILE2_print_WEIGHT (fp, I->pfrq/(2*I->pfrq-I->frq), I->digits, ',');
//    FILE2_putc (fp, ')');
  }
}

/*********************************************************************/
/* add an item to itemset, and update data */
/*********************************************************************/
void LCM_add_item (PROBLEM *PP, QUEUE *Q, QUEUE_INT item){
  QUEUE_INT *x;
  QUE_INS (*Q, item);
  PP->II.itemflag[item] = 1;
  if ( PP->SG.fname )
      MQUE_MLOOP (PP->SG.edge.v[item], x, item) PP->itemary[*x]++;
}

/*********************************************************************/
/* delete an item from itemset, and update data */
/*********************************************************************/
void LCM_del_item (PROBLEM *PP, QUEUE *Q){
  QUEUE_INT *x, item = Q->v[--Q->t];
  PP->II.itemflag[item] = 0;
  if ( PP->SG.fname )
      MQUE_MLOOP (PP->SG.edge.v[item], x, item)  PP->itemary[*x]--;
}

/* remove unnecessary transactions which do not include all posi_closed items */
/* scan of each transaction is up to item */
void LCM_reduce_occ_by_posi_equisupp (PROBLEM *PP, QUEUE *occ, QUEUE_INT item, QUEUE_INT full){
  QUEUE_ID ii=0;
  TRSACT *TT = &PP->TT;
  ITEMSET *II = &PP->II;
  QUEUE_INT *x, *y, *z, cnt;

  MQUE_FLOOP (*occ, x){
    if ( TT->w[*x]>= 0 ) continue;
    cnt = 0;
    MQUE_MLOOP (TT->T.v[*x], y, item) if ( II->itemflag[*y] == 2 ) cnt++;
    if ( cnt==full ) occ->v[ii++] = *x;
    else {
      II->frq -= TT->w[*x];
      MQUE_MLOOP (TT->T.v[*x], z, item) PP->occ_w[*z] -= TT->w[*x];
    }
  }
  occ->t = ii;
  MQUE_FLOOP (PP->itemcand, x){
    if ( II->itemflag[*x] == 2 ) II->itemflag[*x] = 1;
  }
}

/*************************************************************************/
/* ppc check and maximality check */
/* INPUT: O:occurrence, jump:items, th:support, frq:frequency, add:itemset
   OUTPUT: maximum item i s.t. frq(i)=frq
   OPERATION: remove infrequent items from jump, and 
    insert items i to "add" s.t. frq(i)=frq                              */
/*************************************************************************/
/* functions
  1. when closed itemset mining or maximal frequent itemset mining, find all items
   included in all transactions in occ (checked by pfrq, occ_w
   if there is such an item with index>item, ppc condition is violated, and return non-negative value
  2. when constraint graph is given, set the frequency (occ_w) of the items which can
   not be added to itemset to infrequent number.
  3. count the size of reduced database
  4. call LCM_reduce_occ_posi
 */
QUEUE_INT LCM_maximality_check (PROBLEM *PP, QUEUE *occ, QUEUE_INT item, QUEUE_INT *fmax, QUEUE_INT *cnt){
  ITEMSET *II = &PP->II;
  TRSACT *TT = &PP->TT;
  QUEUE_INT m = TT->T.clms, full=0, *x;
  WEIGHT w=-WEIGHTHUGE;
  *fmax = TT->T.clms; *cnt=0;

  MQUE_FLOOP (TT->jump, x){
    if ( II->itemflag[*x] == 1) continue;
//QUEUE_perm_print (&II->itemset, II->perm);
    if ( PP->SG.fname && ( (((PP->problem & LCM_UNCONST)==0) && (PP->itemary[*x]>0) ) || 
        ((PP->problem & LCM_UNCONST) && (PP->itemary[*x]<II->itemset.t ))) ){
      // e can not be added by item constraint
//      PP->occ_pw[e] = PP->occ_w[e] = II->frq_lb -1;
      II->itemflag[*x] = 3;
    } else if ( ISEQUAL(PP->occ_pw[*x],II->pfrq) && ( ISEQUAL(PP->occ_w[*x],II->frq) || (PP->problem & LCM_POSI_EQUISUPP) ) ){ // check e is included in all transactions in occ
      if ( *x<item ){
        if ( !PP->SG.fname ){ // add item as "equisupport"
          LCM_add_item (PP, &II->add, *x);
          if ( (PP->problem&LCM_POSI_EQUISUPP) && (II->flag&ITEMSET_RULE) ) II->itemflag[*x] = 0; // in POSI_EQUISUPP, occ_w[*x] is not equal to II->frq, thus we have to deal it in the rule mining
        }
        if ( !ISEQUAL(PP->occ_w[*x],II->frq) ){ full++; II->itemflag[*x] = 2; }
      } else m = *x; // an item in prefix can be added without going to another closed itemset
    } else {
      if ( *x<item ) (*cnt)++;
      II->itemflag[*x] = PP->occ_pw[*x] < PP->th? 3: 0; // mark item by freq/infreq
      if ( PP->occ_w[*x] > w ){
        *fmax = *x;
        w = PP->occ_w[*x];
      }
    }
  }
  if ( full && (PP->problem & LCM_POSI_EQUISUPP) && m<item ) // m<item always holds in frequent itemset mining
       LCM_reduce_occ_by_posi_equisupp (PP, occ, item, full);
  return (m);
}

/***************************************************************/
/* iteration of LCM ver. 5 */
/* INPUT: item:tail of the current solution, t_new,buf:head of the list of 
 ID and buffer memory of new transactions */
/*************************************************************************/
void LCM (PROBLEM *PP, int item, QUEUE *occ, WEIGHT frq, WEIGHT pfrq, weighted_itemsets *r_wis){
  ITEMSET *II = &PP->II;
  TRSACT *TT = &PP->TT;
  int bnum = TT->buf.num, bblock = TT->buf.block_num;
  int wnum = TT->wbuf.num, wblock = TT->wbuf.block_num;
  VEC_ID new_t = TT->new_t;
  QUEUE_INT cnt, f, *x, m, e, imax = PP->clms? item: TT->T.clms;
  QUEUE_ID js = PP->itemcand.s, qt = II->add.t, i;
  WEIGHT rposi=0.0;

//TRSACT_print (TT, occ, NULL);
//printf ("itemset: %f ::::", II->frq); QUEUE_perm_print (&II->itemset, II->perm);
//QUEUE_print__ ( occ );
//printf ("itemset: %f ::::", II->frq); QUEUE_perm_print (&II->itemset, II->perm);
//printf ("itemset: %f ::::", II->frq); QUEUE_print__ (&II->itemset);
//FLOOP (i, 0, item) printf ("%1.2f, ", PP->occ_w[i]); printf ("\n ### ");
//FLOOP (i, item, II->item_max) printf ("%1.2f, ", PP->occ_w[i]); printf ("\n");

//printf ("add:"); QUEUE_perm_print (&II->add, II->perm);
//for (i=0 ; i<II->imax ; i++ ) printf ("%d(%d) ", II->perm[i], II->itemflag[i]); printf ("\n");

  II->iters++;
  PP->itemcand.s = PP->itemcand.t;
//  if ( II->flag&ITEMSET_POSI_RATIO && pfrq!=0 ) II->frq /= (pfrq+pfrq-II->frq);
  if ( II->flag&ITEMSET_POSI_RATIO && pfrq!=0 ) rposi = pfrq / (pfrq+pfrq-II->frq);
  TRSACT_delivery (TT, &TT->jump, PP->occ_w, PP->occ_pw, occ, imax);
    // if the itemset is empty, set frq to the original #trsactions, and compute item_frq's
  if ( II->itemset.t == 0 ){
    if ( TT->total_w_org != 0.0 )
        FLOOP (i, 0, TT->T.clms) II->item_frq[i] = PP->occ_w[i]/TT->total_w_org;
  }

  II->frq = frq; II->pfrq = pfrq;
  m = LCM_maximality_check (PP, occ, item, &f, &cnt);
// printf ("add: "); QUEUE_print__ ( &II->add);

  if ( !(PP->problem & PROBLEM_FREQSET) && m<TT->T.clms ){  // ppc check
    MQUE_FLOOP (TT->jump, x) TT->OQ[*x].end = 0;
    goto END;
  }
  if ( !(PP->problem&PROBLEM_MAXIMAL) || f>=TT->T.clms || PP->occ_w[f]<II->frq_lb ){
    if ( !(II->flag & ITEMSET_POSI_RATIO) || (rposi<=II->rposi_ub && rposi>=II->rposi_lb) ){
      II->prob = 1.0;
      MQUE_FLOOP (II->itemset, x) II->prob *= II->item_frq[*x];
      MQUE_FLOOP (II->add, x) II->prob *= II->item_frq[*x];
      ITEMSET_check_all_rule_ext(II, PP->occ_w, occ, &TT->jump, TT->total_pw_org, 0, r_wis);      //    if (ERROR_MES) return;
    }
  }
    // select freqeut (and addible) items with smaller indices
  MQUE_FLOOP (TT->jump, x){
    TT->OQ[*x].end = 0;  // in the case of freqset mining, automatically done by rightmost sweep;
    if ( *x<item && II->itemflag[*x] == 0 ){
      QUE_INS (PP->itemcand, *x);
      PP->occ_w2[*x] = PP->occ_w[*x];
      if ( TT->flag2 & TRSACT_NEGATIVE ) PP->occ_pw2[*x] = PP->occ_pw[*x];
    }
  }

  if (ERROR_MES) return; // とりあえずこれで動く
  if ( QUEUE_LENGTH_(PP->itemcand)==0 || II->itemset.t >= II->ub ) goto END;
  qsort_QUEUE_INT (PP->itemcand.v+PP->itemcand.s, PP->itemcand.t-PP->itemcand.s, -1);
//QUEUE_print__ (&PP->itemcand);
  qsort_QUEUE_INT (II->add.v+qt, II->add.t-qt, -1);

// database reduction
  if ( cnt>2 && (II->flag & ITEMSET_TRSACT_ID)==0 && II->itemset.t >0){
    TRSACT_find_same (TT, occ, item);
    TRSACT_merge_trsact (TT, &TT->OQ[TT->T.clms], item);
    TRSACT_reduce_occ (TT, occ);
  }
// occurrence deliver
  TRSACT_deliv (TT, occ, item);

// loop for recursive calls
  cnt = QUEUE_LENGTH_ (PP->itemcand); f=0;   // for showing progress
  while ( QUEUE_LENGTH_ (PP->itemcand) > 0 ){
    e = QUEUE_ext_tail_ (&PP->itemcand);
    if ( PP->occ_pw2[e] >= MAX(II->frq_lb, II->posi_lb) ){  // if the item is frequent
      LCM_add_item (PP, &II->itemset, e);
      LCM (PP, e, &TT->OQ[e], PP->occ_w2[e], PP->occ_pw2[e], r_wis); // recursive call
if ( ERROR_MES ) return;
      LCM_del_item (PP, &II->itemset);
    }
    TT->OQ[e].end = TT->OQ[e].t = 0;   // clear the occurrences, for the further delivery
    PP->occ_w[e] = PP->occ_pw[e] = -WEIGHTHUGE;  // unnecessary?
    
    if ( (II->flag & SHOW_PROGRESS) && (II->itemset.t == 0 ) ){
      f++; print_err ("%d/%d (" LONGF " iterations)\n", f, cnt, II->iters);
    }
  }

  TT->new_t = new_t;
  TT->buf.num = bnum, TT->buf.block_num = bblock;
  TT->wbuf.num = wnum, TT->wbuf.block_num = wblock;

  END:;
  while ( II->add.t > qt ) LCM_del_item (PP, &II->add);
  PP->itemcand.t = PP->itemcand.s;
  PP->itemcand.s = js;
}

/*************************************************************************/
/* initialization of LCM main routine */
/*************************************************************************/
void LCM_init(PROBLEM *PP){
  ITEMSET *II = &PP->II;
  TRSACT *TT = &PP->TT;
  SGRAPH *SG = &PP->SG;
  PERM *sperm = NULL, *tmp=NULL;
  QUEUE_INT i;

  II->X = TT;
  II->flag |= ITEMSET_ITEMFRQ + ITEMSET_ADD;
//  if (!TT->wfname){
//    ENMIN (II->frq_ub, (WEIGHT)TT->T.t); // set  maximum frequency
//    if (II->topk_k > 0) II->flag |= ITEMSET_SC2;
//  }
  PP->clms = ((PP->problem&PROBLEM_FREQSET)&&(II->flag&ITEMSET_RULE)==0);
  PROBLEM_alloc (PP, TT->T.clms, TT->T.t, 0, TT->perm, PROBLEM_ITEMCAND +(PP->SG.fname?PROBLEM_ITEMARY:0) +((TT->flag2&TRSACT_NEGATIVE)?PROBLEM_OCC_PW: PROBLEM_OCC_W) +((PP->problem&PROBLEM_FREQSET)?0:PROBLEM_OCC_W2));
  PP->th = (II->flag&ITEMSET_RULE)? ((II->flag&ITEMSET_RULE_INFRQ)? -WEIGHTHUGE: II->frq_lb * II->ratio_lb ): II->frq_lb;  // threshold for database reduction
  if ( TT->flag2&TRSACT_SHRINK ) PP->oo = QUEUE_dup_ (&TT->OQ[TT->T.clms]); // preserve occ
  else { QUEUE_alloc (&PP->oo, TT->T.t); ARY_INIT_PERM(PP->oo.v, TT->T.t); PP->oo.t = TT->T.t; }
  TT->perm = NULL;
  TT->OQ[TT->T.clms].t = 0;
  print_mes (&PP->TT, "separated at %d\n", PP->TT.sep);

  if ( !(TT->sc) ) calloc2 (TT->sc, TT->T.clms+2, return);
  free2 (II->itemflag); II->itemflag = TT->sc;  // II->itemflag and TT->sc shares the same memory
  II->frq = TT->total_w_org; II->pfrq = TT->total_pw_org;

  if ( PP->SG.fname ){
    if ( SG->edge.t < TT->T.clms )
        print_mes (&PP->TT, "#nodes in constraint graph is smaller than #items\n");
    if ( TT->perm ){
      malloc2 (sperm, SG->edge.t, EXIT);
      ARY_INIT_PERM (sperm, SG->edge.t);
      FLOOP (i, 0, MIN(TT->T.t, SG->edge.t)) sperm[i] = TT->perm[i];
      ARY_INV_PERM (tmp, sperm, SG->edge.t, {free(sperm);EXIT;});
      SGRAPH_replace_index (SG, sperm, tmp);
      mfree (tmp, sperm);
      SG->perm = NULL;
    }
    
    SG->edge.flag |= LOAD_INCSORT +LOAD_RM_DUP;
    SETFAMILY_sort (&SG->edge);
  }
  II->total_weight = TT->total_w;
  if ( (II->flag2&ITEMSET_LAMP) && II->topk.base == 0) II->topk.base = TT->rows_org;
}

void LCM_weighted_itemsets(weighted_itemsets *wis, weighted_itemsets *r_wis, double theta, uint32_t max_itemset_size, uint32_t max_solutions) {
  PROBLEM PP;
  ITEMSET *II = &PP.II;
  TRSACT *TT = &PP.TT;
  
  PROBLEM_init(&PP);
// set LCM parameters
  PP.problem |= PROBLEM_FREQSET;
  II->flag |= ITEMSET_ALL;
  II->flag |= ITEMSET_PRE_FREQ;

  II->max_solutions = max_solutions;
  II->frq_lb = (WEIGHT)theta;
  if (max_itemset_size) II->ub = max_itemset_size;
  PP.output_fname = "/dev/null";

  if (ERROR_MES) exit(1);

  TT->flag |= LOAD_PERM + LOAD_DECSORT + LOAD_RM_DUP;
  TT->flag2 |= TRSACT_FRQSORT + TRSACT_MAKE_NEW + TRSACT_DELIV_SC + TRSACT_ALLOC_OCC + TRSACT_SHRINK + TRSACT_1ST_SHRINK;
  TT->w_lb = II->frq_lb;
  PP.SG.flag =  LOAD_EDGE;
  PROBLEM_load_ext(&PP, wis);

  if ( !ERROR_MES && TT->T.clms>0 ){
    LCM_init (&PP);
    if ( !ERROR_MES ) LCM (&PP, TT->T.clms, &PP.oo, TT->total_w_org, TT->total_pw_org, r_wis);
//    ITEMSET_last_output(II);
  }

  TT->sc = NULL;
  PROBLEM_end(&PP);
}
