"""binarize matrix"""
import unittest
import numpy as np
from scipy.sparse import coo_matrix, csc_matrix, hstack

def binarize_csc_matrix(matrix: csc_matrix, n_bins: int) -> csc_matrix:
    """Binarize scipy.csc_matrix"""
    n_rows, n_cols = matrix.shape
    def binarize_column(col: int) -> csc_matrix:
        """Binarize column"""
        vec = np.reshape(matrix[:, col].toarray(), n_rows)
        values = set()

        for value in vec:
            values.add(value)

        if values <= set({0, 1}):
            return matrix[:, col]

        k = n_bins

        data, rows, cols = [], [], []
        minimum = np.min(vec)
        maximum = np.max(vec)
        step = (maximum - minimum) / k

        for row, value in enumerate(vec):
            col = min(int((value - minimum) / step), k - 1)
            data.append(1)
            rows.append(row)
            cols.append(col)

        shape = vec.shape[0], k
        return coo_matrix((data, (rows, cols)), shape=shape, dtype=int).tocsc()

    mats = list(map(binarize_column, range(n_cols)))
    n_cols = [m.shape[1] for m in mats]

    return hstack(mats), n_cols


class TestBinarize(unittest.TestCase):
    """Unit test"""
    def test_binarize_matrix(self):
        """test binarize_matrix"""
        test_case = np.array([
            [2, 4, 1, ],
            [0, 2, 0, ],
            [-2, 2, 1, ],
        ], dtype=float)
        n_bins = 3
        check = np.array([
            [0, 0, 1, 0, 0, 1, 1, ],
            [0, 1, 0, 1, 0, 0, 0, ],
            [1, 0, 0, 1, 0, 0, 1, ],
        ], dtype=int)
        matrix = csc_matrix(test_case)

        answer = binarize_csc_matrix(matrix, n_bins).todense()
        self.assertTrue((check == answer).all())


if __name__ == '__main__':
    unittest.main()
