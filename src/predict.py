import pickle
from scipy.sparse import csr_matrix, coo_matrix
from sklearn.datasets import load_svmlight_file

def main():
  args = get_args()
  pickle_file_name = args['pickle_file_name']
  data_file_name = args['data_file_name']

  # load data
  x, y = load_svmlight_file(data_file_name)

  with open(pickle_file_name, mode='rb') as f:
    clf = pickle.load(f)

    def get_n_features(clf):
      if hasattr(clf, 'n_features_'):
        return clf.n_features_
      elif hasattr(clf, '_features_count'):
        return clf._features_count
      elif hasattr(clf, 'coef_'):
        return clf.coef_.shape[1]
      else:
        return clf.shape_fit_[1]

    if get_n_features(clf) != x.shape[1]:
      n_rows, n_cols = x.shape[0], get_n_features(clf)
      rows, cols, data = [], [], []
      x = coo_matrix(x)
      for row, col, val in zip(x.row, x.col, x.data):
        if col < n_cols:
          rows.append(row)
          cols.append(col)
          data.append(val)
      x = coo_matrix((data, (rows, cols)), shape=(n_rows, n_cols)).tocsr()

    print('score: ', clf.score(x, y))

def get_args():
  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument('pickle_file_name')
  parser.add_argument('data_file_name')
  return vars(parser.parse_args())

if __name__ == '__main__':
  main()
