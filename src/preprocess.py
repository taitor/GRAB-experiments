"""Preprocess data"""
import sys
import argparse
from scipy.sparse import linalg
from sklearn.datasets import load_svmlight_file, dump_svmlight_file
from mylib.binarize import binarize_csc_matrix


def main():
    """Main function"""
    args = get_args()

    method = args['method']
    input_name = args['input_name']
    output_name = args['output_name']
    binarization_name = args.get('def')

    data = load_svmlight_file(input_name, zero_based=False)

    if len(data) < 2:
        print('Failed to load %s', input_name, file=sys.stderr)
        sys.exit(1)

    data_x = data[0]
    data_y = data[1]

    data_y = convert_data_y(data_y)

    if method == 'binarize':
        data_x = data_x.tocsc()
        n_bins = args['bins']
        bin_x, n_cols = binarize_csc_matrix(data_x, n_bins)
        dump_svmlight_file(bin_x, data_y, output_name, zero_based=False)
        if binarization_name is not None:
            save_binarization(n_cols, binarization_name)


    if method == 'scale':
        # Scale matrix so that the largest norm among records becomes equal to 1
        data_x = data_x.tocsr()
        scaled_x = data_x / linalg.norm(data_x, axis=1).max()
        dump_svmlight_file(scaled_x, data_y, output_name, zero_based=False)


def convert_data_y(data_y):
    """Convert values in data_y to {-1, 1}"""
    values = set()
    for value in data_y:
        values.add(value)

    values = sorted(list(values))

    if len(values) != 2:
        print('The number of value types in data_y have to be 2', file=sys.stderr)
        sys.exit(1)

    data_y[data_y == values[0]] = -1
    data_y[data_y == values[1]] = +1
    return data_y


def check_bins(value):
    """Check bins argument"""
    ivalue = int(value)
    if ivalue < 2:
        raise argparse.ArgumentTypeError('bins should be >= 2')
    return ivalue


def save_binarization(n_cols, file_name):
  file = open(file_name, 'w')
  from_col = 0
  for idx, n_col in enumerate(n_cols):
    to_col = from_col + n_col
    line = f'{idx} => [{from_col}, {to_col})\n'
    file.write(line)
    from_col = to_col


def get_args():
    """Parse arguments"""
    parser = argparse.ArgumentParser(
        description='Preprocess SVMLightFile'
    )
    parser.add_argument('-m', '--method', choices=['binarize', 'scale', ], type=str, required=True)
    parser.add_argument('-b', '--bins', type=check_bins, required=False)
    parser.add_argument('input_name')
    parser.add_argument('output_name')
    parser.add_argument('-d', '--def', required=False)
    return vars(parser.parse_args())


if __name__ == '__main__':
    main()
