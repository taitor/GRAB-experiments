"""Preprocess phishing dataset"""
import argparse
from scipy.sparse import coo_matrix
from sklearn.datasets import load_svmlight_file, dump_svmlight_file

def main():
    """main function"""
    args = get_args()
    data_name = args['data_name']

    data = load_svmlight_file(data_name)
    x, y = data[0].tocoo(), data[1]
    y[y == 0] = -1

    for i in range(len(x.data)):
        x.data[i] = 1

    dump_svmlight_file(x, y, data_name, zero_based=False)


def get_args():
    """Parse arguments"""
    parser = argparse.ArgumentParser(
        description='Preprocess phishing dataset'
    )
    parser.add_argument('data_name')
    return vars(parser.parse_args())


if __name__ == '__main__':
    main()
