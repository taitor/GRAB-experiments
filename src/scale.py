# -*- coding: utf-8 -*-

def main():
  args = get_args()
  input_file_name =  args['input_file']
  scaled_file_name = args['scaled_file']

  import numpy as np
  from sklearn.datasets import load_svmlight_file
  x, y = load_svmlight_file(input_file_name)
  x = x.toarray()
  x = x / np.linalg.norm(x,axis=1).max()
  y = y.astype(int)
  save_svmlight_data(x, y, scaled_file_name)

def get_args():
  import argparse
  parser = argparse.ArgumentParser(
   description='scale SVMLightFile'
  )
  parser.add_argument('input_file')
  parser.add_argument('scaled_file')
  return vars(parser.parse_args())

def save_svmlight_data(data, labels, data_filename, data_folder = ''):
    file = open(data_folder+data_filename,'w')
    for i,x in enumerate(data):
        indexes = x.nonzero()[0]
        values = x[indexes]
        label = '%i'%(labels[i])
        pairs = ['%i:%.10f'%(indexes[i]+1,values[i]) for i in range(len(indexes))]
        sep_line = [label]
        sep_line.extend(pairs)
        sep_line.append('\n')
        line = ' '.join(sep_line)
        file.write(line)

if __name__ == '__main__':
  main()
