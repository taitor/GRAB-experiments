//
//  show_model.cpp
//
//  Created by otita on 2017/01/31.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <string>
#include <unistd.h>

#include "grabc.hpp"


using namespace std;
using namespace otita::grabc;

int main(int argc, char *argv[]) {
  int opt;

  bool mflag = false;
  string method;

  while ((opt = getopt(argc, argv, "m:")) != -1) {
    switch (opt) {
      case 'm':
        mflag = true;
        method = optarg;
        if (!(method == "grab-lr" || method == "grab-svc")) {
          fprintf(stderr, "method must be in (grab-lr, grab-svc)");
          goto USAGE;
        }
        break;
      default: // ?
        goto USAGE;
    }
  }
  char *model_file;

  if (!(mflag && optind < argc)) {
    goto USAGE;
  }

if (0) {
USAGE:
fprintf(stderr, "Usage: %s -m (grab-lr, grab-svc) model_file_name\n",
            argv[0]);
exit(EXIT_FAILURE);
}
  
  model_file = argv[optind];

  if (method == "grab-lr") {
    LogisticRegression clf(model_file);
    clf.print_model();
  }
  else if (method == "grab-svc") {
    SVC clf(model_file);
    clf.print_model();
  }

  return 0;
}
