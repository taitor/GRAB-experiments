//
//  shuffle.cpp
//
//  Created by otita on 2017/01/27.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <random>

#include <cstdint>

#include <unistd.h>

int main(int argc, char *argv[]) {
  using namespace std;
  int opt;

  bool nflag = false;
  uint32_t n = 0;
  bool tflag = false;
  uint32_t t = 0;
  bool sflag = false;
  uint32_t s = 0;
  const char *input_file_name, *train_file_name, *test_file_name;

  while ((opt = getopt(argc, argv, "n:t:s:")) != -1) {
    switch (opt) {
      case 'n':
        nflag = true;
        n = atoi(optarg);
        if (n <= 0) {
          cerr << "condition #(training data) > 0 is not satisfied" << endl;
          goto USAGE;
        }
        break;
      case 't':
        tflag = true;
        t = atoi(optarg);
        if (t <= 0) {
          cerr << "condition #(test data) > 0 is not satisfied" << endl;
          goto USAGE;
        }
        break;
      case 's':
        sflag = true;
        s = atoi(optarg);
        break;
      default: /* '?' */
        goto USAGE;
    }
  }

  if (!(nflag && sflag && optind + 2 < argc)) goto USAGE;

if (0) {
USAGE:
  cerr << "Usage: %s -n #(training data) -s rand_seed [-t #(test data)] input_file_name training_file_name test_file_name" << endl;
  exit(EXIT_FAILURE);
}

  input_file_name = argv[optind];
  train_file_name = argv[optind + 1];
  test_file_name = argv[optind + 2];

  string line;
  vector<string> lines;
  ifstream ifs(input_file_name, ios::in);
  if (!ifs.is_open()) {
    cerr << "cannot open input_file_name: " << input_file_name << endl;
    exit(EXIT_FAILURE);
  }
  while (getline(ifs, line)) lines.push_back(line);

  uint32_t n_train = n;
  uint32_t n_test = tflag ? t : lines.size() - n_train;

  mt19937 engine(s);
  shuffle(lines.begin(), lines.end(), engine);

  ofstream ofs1(train_file_name, ios::out);
  ofstream ofs2(test_file_name, ios::out);
  if (!ofs1.is_open()) {
    cerr << "cannot open training_file_name: " << train_file_name << endl;
    exit(EXIT_FAILURE);
  }
  if (!ofs2.is_open()) {
    cerr << "cannot open test_file_name: " << test_file_name << endl;
    exit(EXIT_FAILURE);
  }
  for (int i = 0; i < n_train; i++) {
    ofs1 << lines[i] << endl;
  }
  for (int i = n_train; i < n_train + n_test; i++) {
    ofs2 << lines[i] << endl;
  }

  return 0;
}
