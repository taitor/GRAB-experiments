"""Shuffle and split data"""
import argparse
import random
import os.path

def main():
    """Main function"""
    args = get_args()
    seed = args['seed']
    trains = args['train']
    test = args['test']
    input_name = args['input_name']
    output_dir = args['output_dir']

    random.seed(seed)

    with open(input_name) as file:
        lines = file.readlines()
        random.shuffle(lines)

        train_lines = lines[:len(lines) - test]
        test_lines = lines[-test:]
        for train in trains:
            random.shuffle(train_lines)
            output_name = os.path.join(output_dir, 'train{}.slf'.format(train))
            with open(output_name, 'w') as output_file:
                output_file.writelines(train_lines[:train])

        output_name = os.path.join(output_dir, 'test.slf')
        with open(output_name, 'w') as output_file:
            output_file.writelines(test_lines)


def get_args():
    """Parse arguments"""
    parser = argparse.ArgumentParser(
        description='Shuffle and split data'
    )
    parser.add_argument('-s', '--seed', required=True)
    parser.add_argument('--train', nargs='+', type=int)
    parser.add_argument('--test', type=int)
    parser.add_argument('input_name')
    parser.add_argument('output_dir')
    return vars(parser.parse_args())


if __name__ == '__main__':
    main()
