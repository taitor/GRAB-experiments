//
//  slf_util.hpp
//
//  Created by otita on 2016/11/29.
//
/*
The MIT License (MIT)

Copyright (c) 2016 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef _SLF_UTIL_HPP_
#define _SLF_UTIL_HPP_

#include <vector>
#include <utility>
#include <fstream>
#include <sstream>
#include <memory>

#include <cstdint>

namespace otita {

template <typename xtype, typename ytype>
void read_slf(
  const char fname[],
  std::vector<std::vector<std::pair<uint32_t, xtype> > > *x_storage,
  std::vector<ytype> *y_storage,
  uint32_t *n_row,
  uint32_t *n_col
) {
  using namespace std;
  ifstream ifs(fname, std::ios::in);
  if (!ifs.is_open()) return;

  *n_row = 0;
  *n_col = 0;

  string line;
  // count rows and cols
  int max_col = 0;
  while (getline(ifs, line)) {
    istringstream iss(line);
    string unit;

    double label;
    vector<pair<uint32_t, xtype> > x_row;
    getline(iss, unit, ' ');
    label = atof(unit.c_str());
    y_storage->push_back(label);

    while (getline(iss, unit, ' ')) {
      istringstream iss2(unit);
      string e;
      int col;
      double val;
      getline(iss2, e, ':');
      col = atoi(e.c_str()) - 1;
      if (col > max_col) max_col = col;
      getline(iss2, e);
      val = atof(e.c_str());
      x_row.push_back(make_pair(col, xtype(val)));
    }
    x_storage->push_back(move(x_row));

    (*n_row)++;
  }
  *n_col = max_col + 1;
}

}

#endif // _SLF_UTIL_HPP_
