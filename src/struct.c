//
//  struct.c
//
//  Created by otita on 2017/01/10.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <stdlib.h>
#include <stdio.h>

#include "struct.h"

void double_vector_init(double_vector *v, uint32_t n_buf) {
  v->n = 0;
  v->n_buf = n_buf;
  v->values = v->n_buf ? (double *)malloc(sizeof(double) * n_buf) : NULL;
  if (v->n_buf && !v->values) {
    fprintf(stderr, "double_vector_init: memory allocate error\n");
    exit(1);
  }
}

void double_vector_free(double_vector *v) {
  free(v->values);
  v->n = 0;
  v->n_buf = 0;
  v->values = NULL;
}

void double_vector_add(double_vector *v, double value) {
  if (!v->values) {
    double_vector_init(v, 1);
  }
  if (v->n < v->n_buf) {
    v->values[v->n++] = value;
  }
  else {
    v->n_buf <<= 1;
    v->values = (double *)realloc(v->values, sizeof(double) * v->n_buf);
    if (!v->values) {
      fprintf(stderr, "double_vector_add: memory allocate error\n");
      exit(1);
    }
    v->values[v->n++] = value;
  }
}

void uint32_vector_init(uint32_vector *v, uint32_t n_buf) {
  v->n = 0;
  v->n_buf = n_buf;
  v->values = v->n_buf ? (uint32_t *)malloc(sizeof(uint32_t) * n_buf) : NULL;
  if (v->n_buf && !v->values) {
    fprintf(stderr, "double_vector_init: memory allocate error\n");
    exit(1);
  }
}

extern void uint32_vector_free(uint32_vector *v) {
  free(v->values);
  v->n = 0;
  v->n_buf = 0;
  v->values = NULL;
}

void uint32_vector_add(uint32_vector *v, uint32_t value) {
  if (!v->values) {
    uint32_vector_init(v, 1);
  }
  if (v->n < v->n_buf) {
    v->values[v->n++] = value;
  }
  else {
    v->n_buf <<= 1;
    v->values = (uint32_t *)realloc(v->values, sizeof(uint32_t) * v->n_buf);
    if (!v->values) {
      fprintf(stderr, "uint32_vector_add: memory allocate error\n");
      exit(1);
    }
    v->values[v->n++] = value;
  }
}

void weighted_itemsets_init(weighted_itemsets *wis, uint32_t n_itemset, uint32_t n_item) {
  uint32_vector_init(&wis->items, n_item);
  uint32_vector_init(&wis->indexes, n_itemset);
  double_vector_init(&wis->weights, n_itemset);
}

void weighted_itemsets_free(weighted_itemsets *wis) {
  uint32_vector_free(&wis->indexes);
  uint32_vector_free(&wis->items);
  double_vector_free(&wis->weights);
}

void weighted_itemsets_add_item(weighted_itemsets *wis, uint32_t item) {
  uint32_vector_add(&wis->items, item);
  wis->indexes.values[wis->indexes.n - 1]++;
}

void weighted_itemsets_add_itemset(weighted_itemsets *wis, double weight) {
  uint32_vector_add(&wis->indexes, wis->indexes.n ? wis->indexes.values[wis->indexes.n - 1] : 0);
  double_vector_add(&wis->weights, weight);
}

void weighted_itemsets_print(weighted_itemsets *wis) {
  uint32_vector *items = &wis->items;
  uint32_vector *indexes = &wis->indexes;
  double_vector *weights = &wis->weights;
  for (int i = 0; i < weights->n; i++) {
    printf("(%lf) ", weights->values[i]);
    for (int j = i ? indexes->values[i - 1] : 0; j < indexes->values[i]; j++) {
      printf("%d ", items->values[j]);
    }
    printf("\n");
  }
  fflush(stdout);
}
