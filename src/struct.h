//
//  struct.h
//
//  Created by otita on 2017/01/10.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef _STRUCT_H_
#define _STRUCT_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct double_vector_t {

  uint32_t n;
  uint32_t n_buf;
  double *values;

} double_vector;

typedef struct uint32_vector_t {

  uint32_t n;
  uint32_t n_buf;
  uint32_t *values;

} uint32_vector;

typedef struct weighted_itemsets_t {

  uint32_vector items;
  uint32_vector indexes;
  double_vector weights;

} weighted_itemsets;

extern void double_vector_init(double_vector *v, uint32_t n_buf);
extern void double_vector_free(double_vector *v);
extern void double_vector_add(double_vector *v, double value);

extern void uint32_vector_init(uint32_vector *v, uint32_t n_buf);
extern void uint32_vector_free(uint32_vector *v);
extern void uint32_vector_add(uint32_vector *v, uint32_t value);

extern void weighted_itemsets_init(weighted_itemsets *wis, uint32_t n_itemset, uint32_t n_item);
extern void weighted_itemsets_free(weighted_itemsets *wis);
extern void weighted_itemsets_add_item(weighted_itemsets *wis, uint32_t item);
extern void weighted_itemsets_add_itemset(weighted_itemsets *wis, double weight);
extern void weighted_itemsets_print(weighted_itemsets *wis);

#ifdef __cplusplus
}
#endif

#endif // _STRUCT_H_
