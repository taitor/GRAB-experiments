//
//  time.cpp
//
//  Created by otita on 2017/01/27.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include <chrono>

#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <cerrno>

#include <unistd.h>
#include <Eigen/Dense>
#include <Eigen/SparseCore>

#include "slf_util.hpp"
#include "linear.h"
#include "grabc.hpp"
#include "grafting.hpp"

#define Malloc(type,n) (type *)malloc((n)*sizeof(type))
#define INF HUGE_VAL

using namespace std;
using namespace Eigen;
using namespace otita;

chrono::milliseconds fitting_time_grab(const char fname[], double C, uint32_t l);
chrono::milliseconds fitting_time_grafting(const char fname[], double C, uint32_t l);
chrono::milliseconds fitting_time_liblinear(const char fname[], double C, uint32_t l);

int main(int argc, char *argv[]) {
  int opt;

  bool mflag = false;
  string method;
  bool Cflag = false;
  double C = 0;
  bool lflag = false;
  uint32_t l = 0;

  const char *input_file_name, *output_file_name;

  while ((opt = getopt(argc, argv, "m:C:l:")) != -1) {
    switch (opt) {
      case 'm':
        mflag = true;
        method = optarg;
        if (!(method == "grab" || method == "grafting" || method == "liblinear")) {
          cerr << "method must be in (grab, grafting, liblinear)" << endl;
          goto USAGE;
        }
        break;
      case 'C':
        Cflag = true;
        C = atof(optarg);
        if (C <= 0) {
          cerr << "condition C > 0 is not satisfied" << endl;
          goto USAGE;
        }
        break;
      case 'l':
        lflag = true;
        int ll;
        ll = atoi(optarg);
        if (ll < 0) {
          cerr << "condition l >= 0 is not satisfied" << endl;
          goto USAGE;
        }
        l = ll;
        break;
      default: /* '?' */
        goto USAGE;
    }
  }

  if (!(mflag && Cflag && lflag && optind + 1 < argc)) goto USAGE;

if (0) {
USAGE:
  cerr << "Usage: " << argv[0] << " -m (grab, grafting, liblinear) -C C -l l data_file_name result_file_name" << endl;
  exit(EXIT_FAILURE);
}

  input_file_name = argv[optind];
  output_file_name = argv[optind + 1];

  ofstream ofs(output_file_name, ios::out);
  if (!ofs.is_open()) {
    cerr << "cannot open output_file_name: " << output_file_name << endl;
    exit(EXIT_FAILURE);
  }

  chrono::milliseconds ms;
  if (method == "grab") {
    ms = fitting_time_grab(input_file_name, C, l);
  }
  else if (method == "grafting") {
    ms = fitting_time_grafting(input_file_name, C, l);
  }
  else if (method == "liblinear") {
    ms = fitting_time_liblinear(input_file_name, C, l);
  }

  ofs << "data=" << input_file_name << endl;
  ofs << "method=" << method << endl;
  ofs << "C=" << C << endl;
  ofs << "l=" << l << endl;
  ofs << "milliseconds=" << ms.count() << endl;

  return 0;
}

void read_slf(const char fname[], SparseMatrix<bool> &x, VectorXi &y);

chrono::milliseconds fitting_time_grab(const char fname[], double C, uint32_t l) {
  using namespace otita::grabc;
  SparseMatrix<bool> x; VectorXi y;
  read_slf(fname, x, y);
  LogisticRegression clf(C, l, 100, {0, 0}, 0.01, 0);
  auto start = chrono::system_clock::now();
  clf.fit(x, y);
  auto end = chrono::system_clock::now();
  return chrono::duration_cast<std::chrono::milliseconds>(end - start);
}

chrono::milliseconds fitting_time_grafting(const char fname[], double C, uint32_t l) {
  using namespace otita::grafting;
  SparseMatrix<bool> x; VectorXi y;
  read_slf(fname, x, y);
  LogisticRegression clf(C, l, 100, {0, 0}, 0.01, 0);
  auto start = chrono::system_clock::now();
  clf.fit(x, y);
  auto end = chrono::system_clock::now();
  return chrono::duration_cast<std::chrono::milliseconds>(end - start);
}

void read_slf(const char fname[], SparseMatrix<bool> &x, VectorXi &y) {
  vector<vector<pair<uint32_t, bool> > > x_storage;
  vector<double> y_storage;
  uint32_t n_row, n_col;
  read_slf(fname, &x_storage, &y_storage, &n_row, &n_col);

  if (!(n_row && n_col)) {
    cerr << "cannot read input_file_name: " << fname << endl;
    exit(EXIT_FAILURE);
  }

  x = SparseMatrix<bool>(n_row, n_col);
  y = VectorXi(n_row);

  vector<Triplet<bool> > x_triplets;
  for (int row = 0; row < n_row; row++) {
    for (auto pair : x_storage[row]) {
      uint32_t col = pair.first;
      bool val = pair.second;

      x_triplets.push_back((Triplet<bool>(row, col, val)));
    }
    y(row) = y_storage[row] > 0 ? +1 : -1;
  }
  x.setFromTriplets(x_triplets.begin(), x_triplets.end());
}

struct feature_node *x_space;
struct parameter param;
struct problem prob;
struct model* model_;
int flag_cross_validation;
int flag_find_C;
int flag_C_specified;
int flag_solver_specified;
int nr_fold;
double bias;

static char *line = NULL;
static int max_line_len;

void exit_input_error(int line_num)
{
	fprintf(stderr,"Wrong input format at line %d\n", line_num);
	exit(1);
}

static char* readline(FILE *input)
{
	int len;

	if(fgets(line,max_line_len,input) == NULL)
		return NULL;

	while(strrchr(line,'\n') == NULL)
	{
		max_line_len *= 2;
		line = (char *) realloc(line,max_line_len);
		len = (int) strlen(line);
		if(fgets(line+len,max_line_len-len,input) == NULL)
			break;
	}
	return line;
}

void read_problem(const char *filename);
void print_null(const char *s) {}

chrono::milliseconds fitting_time_liblinear(const char fname[], double C, uint32_t l) {
  read_problem(fname);
  param.solver_type = L1R_LR;
	param.C = C;
	param.eps = 0.01; // see setting below
	param.p = 0.1;
	param.nr_weight = 0;
	param.weight_label = NULL;
	param.weight = NULL;
	param.init_sol = NULL;
	flag_cross_validation = 0;
	flag_C_specified = 0;
	flag_solver_specified = 0;
	flag_find_C = 0;
	bias = -1;
	void (*print_func)(const char*) = &print_null;	// default printing to stdout
	set_print_string_function(print_func);

  auto start = chrono::system_clock::now();
	model_=train(&prob, &param);
  auto end = chrono::system_clock::now();
	destroy_param(&param);
	free(prob.y);
	free(prob.x);
	free(x_space);
	free(line);
  return chrono::duration_cast<std::chrono::milliseconds>(end - start);
}

void read_problem(const char *filename)
{
	int max_index, inst_max_index, i;
	size_t elements, j;
	FILE *fp = fopen(filename,"r");
	char *endptr;
	char *idx, *val, *label;

	if(fp == NULL)
	{
		fprintf(stderr,"can't open input file %s\n",filename);
		exit(1);
	}

	prob.l = 0;
	elements = 0;
	max_line_len = 1024;
	line = Malloc(char,max_line_len);
	while(readline(fp)!=NULL)
	{
		char *p = strtok(line," \t"); // label

		// features
		while(1)
		{
			p = strtok(NULL," \t");
			if(p == NULL || *p == '\n') // check '\n' as ' ' may be after the last feature
				break;
			elements++;
		}
		elements++; // for bias term
		prob.l++;
	}
	rewind(fp);

	prob.bias=bias;

	prob.y = Malloc(double,prob.l);
	prob.x = Malloc(struct feature_node *,prob.l);
	x_space = Malloc(struct feature_node,elements+prob.l);

	max_index = 0;
	j=0;
	for(i=0;i<prob.l;i++)
	{
		inst_max_index = 0; // strtol gives 0 if wrong format
		readline(fp);
		prob.x[i] = &x_space[j];
		label = strtok(line," \t\n");
		if(label == NULL) // empty line
			exit_input_error(i+1);

		prob.y[i] = strtod(label,&endptr);
		if(endptr == label || *endptr != '\0')
			exit_input_error(i+1);

		while(1)
		{
			idx = strtok(NULL,":");
			val = strtok(NULL," \t");

			if(val == NULL)
				break;

			errno = 0;
			x_space[j].index = (int) strtol(idx,&endptr,10);
			if(endptr == idx || errno != 0 || *endptr != '\0' || x_space[j].index <= inst_max_index)
				exit_input_error(i+1);
			else
				inst_max_index = x_space[j].index;

			errno = 0;
			x_space[j].value = strtod(val,&endptr);
			if(endptr == val || errno != 0 || (*endptr != '\0' && !isspace(*endptr)))
				exit_input_error(i+1);

			++j;
		}

		if(inst_max_index > max_index)
			max_index = inst_max_index;

		if(prob.bias >= 0)
			x_space[j++].value = prob.bias;

		x_space[j++].index = -1;
	}

	if(prob.bias >= 0)
	{
		prob.n=max_index+1;
		for(i=1;i<prob.l;i++)
			(prob.x[i]-2)->index = prob.n;
		x_space[j-2].index = prob.n;
	}
	else
		prob.n=max_index;

	fclose(fp);
}
