//
//  train.cpp
//
//  Created by otita on 2017/01/31.
//
/*
The MIT License (MIT)

Copyright (c) 2017 otita.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <unistd.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <chrono>

#include "random.hpp"
#include "grabc.hpp"
#include "slf_util.hpp"

using namespace std;
using namespace Eigen;
using namespace otita;
using namespace otita::grabc;

void load_data(
  const vector<vector<pair<uint32_t, bool> > > &x_storage,
  const vector<double> &y_storage,
  uint32_t n_rows,
  uint32_t n_cols,
  X &x,
  Y &y
);

int main(int argc, char *argv[]) {

  int opt;

  bool mflag = false;
  string method;
  bool Cflag = false;
  double C = 0;
  double Cp = 0;
  double Cn = 0;
  bool kflag = false;
  uint32_t k = 0;
  bool lflag = false;
  uint32_t l = 0;
  double eps = 0.01;
  char *input_file, *output_file;

  while ((opt = getopt(argc, argv, "m:C:p:n:k:l:e:")) != -1) {
    switch (opt) {
      case 'm':
        mflag = true;
        method = optarg;
        if (!(method == "grab-lr" || method == "grab-svc")) {
          fprintf(stderr, "method must be in (grab-lr, grab-svc)");
          goto USAGE;
        }
        break;
      case 'C':
        Cflag = true;
        C = atof(optarg);
        if (C <= 0) {
          fprintf(stderr, "condition C > 0 is not satisfied\n");
          goto USAGE;
        }
        break;
      case 'p':
        Cp = atof(optarg);
        if (Cp <= 0) {
          fprintf(stderr, "condition Cp > 0 is not satisfied\n");
          goto USAGE;
        }
        break;
      case 'n':
        Cn = atof(optarg);
        if (Cn <= 0) {
          fprintf(stderr, "condition Cn > 0 is not satisfied\n");
          goto USAGE;
        }
        break;
      case 'k': {
        kflag = true;
        int kk = atoi(optarg);
        if (kk <= 0) {
          fprintf(stderr, "condition k > 0 is not satisfied\n");
          goto USAGE;
        }
        k = kk;
        break;
      }
      case 'l': {
        lflag = true;
        int ll = atoi(optarg);
        if (ll < 0) {
          fprintf(stderr, "condition l >= 0 is not satisfied\n");
          goto USAGE;
        }
        l = ll;
        break;
      }
      case 'e': {
        eps = atof(optarg);
        if (eps <= 0) {
          fprintf(stderr, "condition eps > 0 is not satisfied\n");
          goto USAGE;
        }
        break;
      }
      default: /* '?' */
        goto USAGE;
    }
  }

  if (!(mflag && Cflag && kflag && lflag && lflag && optind + 1 < argc)) {
    goto USAGE;
  }

if (0) {
USAGE:
fprintf(stderr, "usage: %s -m (grab-lr, grab-svc) -C C] [-p Cp] [-n Cn] -k #(new features at once) -l (max feature length) [-e tolerance] data_file_name model_file_name\n",
            argv[0]);
exit(EXIT_FAILURE);
}
 
  input_file = argv[optind];
  output_file = argv[optind + 1];

  X x; Y y;

  uint32_t n_rows, n_cols;
  vector<vector<pair<uint32_t, bool> > > x_storage;
  vector<double> y_storage;
  read_slf(input_file, &x_storage, &y_storage, &n_rows, &n_cols);

  load_data(x_storage, y_storage, n_rows, n_cols, x, y);

  double score = 0;
  auto start = chrono::system_clock::now();
  if (method == "grab-lr") {
    LogisticRegression clf(C, l, k, {Cp, Cn}, eps, 0);
    clf.fit(x, y);
    clf.write_model(output_file);
    score = clf.score(x, y);
  }
  else if (method == "grab-svc") {
    SVC clf(C, l, k, {Cp, Cn}, eps, 0);
    clf.fit(x, y);
    clf.write_model(output_file);
    score = clf.score(x, y);
  }
  auto end = chrono::system_clock::now();

  printf("score: %lf\n", score);
  cout << "milliseconds: " << chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << endl;

  return 0;
}

void load_data(
  const vector<vector<pair<uint32_t, bool> > > &x_storage,
  const vector<double> &y_storage,
  uint32_t n_rows,
  uint32_t n_cols,
  X &x,
  Y &y
) {
  x = X(n_rows, n_cols);
  y = Y(n_rows);

  vector<Triplet<bool> > x_triplets;
  for (int i = 0; i < n_rows; i++) {
    for (auto pair : x_storage[i]) {
      uint32_t col = pair.first;
      bool val = pair.second;

      x_triplets.push_back((Triplet<bool>(i, col, val)));
    }
    y(i) = y_storage[i] > 0 ? +1 : -1;
  }
  x.setFromTriplets(x_triplets.begin(), x_triplets.end());
}
