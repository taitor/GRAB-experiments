"""Train model"""
import time
import pickle
import argparse
from sklearn import svm, tree, ensemble
from sklearn.datasets import load_svmlight_file
from xgboost import XGBClassifier
from sklearn.linear_model import LogisticRegression

def main():
    """Main function"""
    args = get_args()
    input_file_name = args['input_file_name']
    output_file_name = args['output_file_name']
    method = args['method']

    data = load_svmlight_file(input_file_name)
    x = data[0].astype(float)
    y = data[1].astype(int)

    current_milli_time = lambda: int(round(time.time() * 1000))

    if method == 'svc':
        C = float(args['C'])
        kernel = args['kernel']

        if kernel == 'linear':
            clf = svm.SVC(C=C, kernel=kernel)

        elif kernel == 'rbf':
            gamma = args['gamma']
            if gamma != 'auto':
                gamma = float(gamma)

            clf = svm.SVC(C=C, kernel=kernel, gamma=gamma)

        elif kernel == 'poly':
            degree = int(args['degree'])
            gamma = float(args['gamma'])
            coef0 = 1 - gamma

            clf = svm.SVC(C=C, kernel=kernel, degree=degree, gamma=gamma, coef0=coef0)

    elif method == 'tree':
        clf = tree.DecisionTreeClassifier(
            random_state=0, max_depth=int(args['max_depth'])
        )

    elif method == 'forest':
        clf = ensemble.RandomForestClassifier(
            random_state=0, n_estimators=int(args['n_estimators']),
            max_depth=int(args['max_depth'])
        )

    elif method == 'xgboost':
        n = x.shape[0]
        reg_lambda = float(1) / (n * float(args['C']))
        clf = XGBClassifier(
            max_depth=int(args['max_depth']),
            reg_lambda=reg_lambda,
            n_jobs=1,
            random_state=0
        )

    elif method == 'l1lr':
        clf = LogisticRegression(
            penalty='l1',
            solver='liblinear',
            random_state=0,
            C=float(args['C']),
            fit_intercept=False
        )

    start = current_milli_time()
    clf.fit(x, y)
    end = current_milli_time()

    print('score: ', clf.score(x, y))
    print('milliseconds: ', end - start)

    with open(output_file_name, mode='wb') as f:
        pickle.dump(clf, f)

def get_args():
    """Parse arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file_name')
    parser.add_argument('output_file_name')
    parser.add_argument('-C', '--C', required=False)
    parser.add_argument('-m', '--method', required=True)
    parser.add_argument('-k', '--kernel', required=False)
    parser.add_argument('-d', '--degree', required=False)
    parser.add_argument('-g', '--gamma', required=False)
    parser.add_argument('--n_estimators', required=False)
    parser.add_argument('--max_depth', required=False)
    return vars(parser.parse_args())

if __name__ == '__main__':
    main()
