"""Get data information"""
import numpy as np
from sklearn.datasets import load_svmlight_file

def main():
    """main function"""
    args = get_args()
    data_file_name = args['data_file_name']

    data = load_svmlight_file(data_file_name)
    data_x = data[0].tocoo()
    data_y = data[1]

    n_rows, n_cols = data_x.shape
    n_pos, n_neg = len(data_y[data_y == +1]), len(data_y[data_y == -1])

    print("data name = {}".format(data_file_name))
    print("(n_rows, n_cols) = ({}, {})".format(n_rows, n_cols))
    print("sparsity = {}".format(get_sparsity(data_x)))
    print("pos = {}, neg = {}, rate = {}".format(n_pos, n_neg, n_pos / (n_pos + n_neg)))
    print("values:")
    for i, (a, b) in enumerate(get_values(data_x)):
        print("{}: [{}, {}]".format(i, a, b))


def get_values(matrix):
    x = matrix.tocsc()
    n_cols = x.shape[1]
    a = []
    for col in range(n_cols):
        v = x[:, col]
        a.append((v.min(), v.max()))
    return a


def get_sparsity(matrix):
    """Return sparsity of a sparse matrix"""
    shape = matrix.shape
    n_elements = shape[0] * shape[1]
    n_nonzeros = matrix.count_nonzero()
    return n_nonzeros / n_elements


def get_args():
    """Parse arguments"""
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('data_file_name')
    return vars(parser.parse_args())


if __name__ == '__main__':
    main()
