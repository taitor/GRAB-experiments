#!/bin/bash

METHOD=${1}

if [[ $METHOD =~ ^grab ]]; then
  DATA_TYPE="binarize"
  BASE="../src/predict -m ${METHOD}"
else
if [[ $METHOD = l1lr ]]; then
  DATA_TYPE="fim"
  BASE="python3 ../src/predict.py"
else
  DATA_TYPE="binscale"
  BASE="python3 ../src/predict.py"
fi
fi

for data in `echo $(ls -d */) | sed 's/\///g'`; do
  for model in `ls ${data}/${DATA_TYPE}/${METHOD}*`; do
    res=`${BASE} ${model} ../data/${data}/${DATA_TYPE}/test.slf`
    echo "${data} ${res}"
  done
done

